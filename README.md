# `dotfiles`

## Files that need manual intervention

- `~/.docker/config.json`

## `~/.docker`

### Just put this config inside `~/.docker/.config.json`

```json
{
    "credsStore": "pass"
}
```

### Git options to set username and email

To set user and email globally

```sh
  git config --global user.email "you@example.com"
  git config --global user.name "Your Name"
```

To set user and email locally for repo

```sh
  git config user.email "you@example.com"
  git config user.name "Your Name"
```
