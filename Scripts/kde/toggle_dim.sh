#!/bin/sh

change_config() {
	kwriteconfig6 --file kwinrc --group Plugins --key diminactiveEnabled --type bool "$1"
	qdbus org.kde.KWin /KWin reconfigure
}

if [ "$1" = "enable" ]; then
	change_config true
elif [ "$1" = "disable" ]; then
	change_config false
fi

curr_value="$(kreadconfig6 --file kwinrc --group Plugins --key diminactiveEnabled)"

if [ "$curr_value" = true ]; then
	change_config false
else
	change_config true
fi
