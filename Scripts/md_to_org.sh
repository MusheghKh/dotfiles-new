#!/bin/sh

find . -name '*.md' | while read -r f; do
  pandoc -f markdown -t org -o "${f}".org "${f}"
done
