#!/usr/bin/perl

# Usage   ./shift_srt.pl <time> <file>
# Example ./shift_srt.pl 1.100 sub.srt
use warnings;
use strict;

use constant FACTORS => ( 60 * 60 * 1000, 60 * 1000, 1000, 1 );

sub time2ms {
    my $time = shift;
    my ( $ms, $i ) = ( 0, 0 );
    $ms += (FACTORS)[ $i++ ] * $_ for split /[^0-9]/, $time;
    return $ms;
}

sub ms2time {
    my $ms  = shift;
    my $str = q();
    for my $i ( 0 .. 3 ) {
        $str .= sprintf +( $i == 3 ? '%03d' : '%02d' )
          . ( ':', ':', ',', q() )[$i],
          $ms / (FACTORS)[$i];
        $ms = $ms % (FACTORS)[$i];
    }
    return $str;
}

my $diff   = 1000 * shift;
my $TIME_R = qr/[0-9]{2}:[0-9]{2}:[0-9]{2},[0-9]{3}/;
while (<>) {
    if ( my ( $from, $to ) = /($TIME_R) --> ($TIME_R)/ ) {
        my $i = 0;
        for my $time ( $from, $to ) {
            $time = time2ms($time) + $diff;
            print ms2time($time), ( ' --> ', "\n" )[ $i++ ];
        }
    }
    else {
        print;
    }
}
