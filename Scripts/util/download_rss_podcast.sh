#!/bin/sh

set -e

if [ "$1" = "--help" ]; then
	echo "$0 <url> [count of titles to skip]"
	exit 1
fi

if [ -z "$1" ]; then
	echo "url not specified" 1>&2
	exit 1
fi

if [ -n "$2" ]; then
	skip_titles="$2"
	if echo "$skip_titles" | grep -E "[^0-9]"; then
		echo wring number of titles to skip
		echo expected number
		exit 1
	fi
	echo "skipping $skip_titles titles"
else
	echo not skipping titles
fi

echo downloading rss feed
curl -L -o /tmp/rss.txt "$1"

if [ -n "$skip_titles" ]; then
	grep -o '<title>.*' /tmp/rss.txt | tail -n "+$skip_titles" | sed -e "s/<title>//" -e "s/<\/title>//" > /tmp/titles.txt
else
	grep -o '<title>.*' /tmp/rss.txt | sed -e "s/<title>//" -e "s/<\/title>//" > /tmp/titles.txt
fi

grep -o '<enclosure url="[^"]*' /tmp/rss.txt | sed 's/<enclosure url="//' > /tmp/url.txt

pr -J -m -t /tmp/url.txt /tmp/titles.txt > /tmp/format.txt

echo
echo Starting downloading
while IFS= read -r line; do
	url="$(echo "$line" | awk '{print $1}')"
	title="$(echo "$line" | awk '{$1=""; print $0; }' | sed 's/^[[:blank:]]*//;s/[[:blank:]]*$//')"
	echo "$url"
	echo "$title"
	curl -L -o "$title.mp3" "$url"
done < /tmp/format.txt

echo
echo Cleaning up
rm /tmp/rss.txt /tmp/titles.txt /tmp/url.txt /tmp/format.txt
