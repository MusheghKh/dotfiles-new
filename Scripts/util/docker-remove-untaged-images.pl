#!/usr/bin/env perl

use strict;
use warnings;

use Data::Dumper;

my @images = `docker images`;

my @nones;

foreach (@images) {
    if ( $_ =~ /^([^ ]+)\s+<none>\s+(\w+)\s+.*/ ) {

        # $1 = image repository (name)
        # $2 = image id
        push( @nones, [ $1, $2 ] );
    }
}

my $size = @nones;
if ( $size == 0 ) {
    print STDERR "No untaged images to delete\n";
    exit 1;
}

if ( $size != 0 ) {
    my @image_names;
    my @image_ids;
    foreach (@nones) {
        push( @image_names, $_->[0] );
        push( @image_ids,   $_->[1] );
    }
    print "Do you want to delete this untaged images [@image_names] (y/n): ";
    my $answer = <STDIN>;
    chomp($answer);
    if ( $answer ne 'y' && $answer ne 'Y' ) {
        print STDERR "Canceled\n";
        exit 1;
    }
    chomp(@image_ids);
    my $image_ids_str = join( " ", @image_ids );
    my $result        = system("docker rmi $image_ids_str");
    if ( $result != 0 ) {
        print STDERR "Can't delete images\n";
        exit 1;
    }
    print "Deleted sucessfuly\n";
}
