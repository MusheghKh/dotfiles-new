#!/bin/sh

set -e

install_plugin() {
	url="$1"

	name="$(basename "$1")"
	name="${name%.*}"

	if [ ! -d "$HOME/.zsh/plugins/$name" ]; then
		cd ~/.zsh/plugins || exit 1
		git clone --depth 1 --shallow-submodules "$url"
	else
		cd ~/.zsh/plugins/"$name" || exit 1
		git pull
	fi
}

mkdir -p ~/.zsh/plugins

install_plugin "https://github.com/zsh-users/zsh-autosuggestions.git"
install_plugin "https://github.com/zsh-users/zsh-syntax-highlighting.git"
