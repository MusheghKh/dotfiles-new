#!/bin/sh

mkdir -p ~/.tmux/plugins
cd ~/.tmux/plugins || exit 1

[ -d "tpm" ] || git clone https://github.com/tmux-plugins/tpm
