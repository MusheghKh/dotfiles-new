vim.api.nvim_create_augroup("User_startup_autgroup_repo", { clear = true })
vim.api.nvim_create_autocmd("User", {
	group = "User_startup_autgroup_repo",
	pattern = "VeryLazy",
	callback = function ()
		print("startup finished")
	end
})
