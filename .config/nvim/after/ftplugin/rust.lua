vim.api.nvim_create_augroup("User_rust_startu_autogroup", { clear = true })
vim.api.nvim_create_autocmd("User", {
	group = "User_rust_startu_autogroup",
	pattern = "VeryLazy",
	callback = function()
		vim.cmd("Lazy load neotest")
		vim.cmd("Lazy load mason-nvim-dap.nvim")
	end,
})

local bufnr = vim.api.nvim_get_current_buf()

--
-- codeAction
--
vim.keymap.set("n", "<leader>ax", function()
	vim.cmd.RustLsp("codeAction") -- supports rust-analyzer's grouping
end, { silent = true, buffer = bufnr, desc = "Code Action" })

--
-- debug
--
vim.keymap.set("n", "<leader>aR", function()
	vim.cmd.RustLsp("debug")
end, { silent = true, buffer = bufnr, desc = "Debug" })
-- vim.cmd.RustLsp('debug')
-- vim.cmd.RustLsp('debuggables')
-- -- or, to run the previous debuggable:
-- vim.cmd.RustLsp { 'debuggables', bang = true }
-- -- or, to override the executable's args:
-- vim.cmd.RustLsp {'debuggables', 'arg1', 'arg2' }

--
-- runnables
--
vim.keymap.set("n", "<leader>ar", function()
	vim.cmd.RustLsp("run")
end, { silent = true, buffer = bufnr })
-- vim.cmd.RustLsp('run')
-- vim.cmd.RustLsp('runnables')
-- -- or, to run the previous runnable:
-- vim.cmd.RustLsp { 'runnables', bang = true }
-- -- or, to override the executable's args:
-- vim.cmd.RustLsp {'runnables', 'arg1', 'arg2' }

--
-- test
--
-- vim.cmd.RustLsp('testables')
-- -- or, to run the previous testables:
-- vim.cmd.RustLsp { 'testables', bang = true }
-- -- or, to override the executable's args:
-- vim.cmd.RustLsp {'testables', 'arg1', 'arg2' }

--
-- expand macros
--
vim.keymap.set("n", "<leader>am", function()
	vim.cmd.RustLsp("expandMacro")
end, { silent = true, buffer = bufnr, desc = "Expand Macro" })

--
-- rebuild macros
--
vim.keymap.set("n", "<leader>aM", function()
	vim.cmd.RustLsp("rebuildProcMacros")
end, { silent = true, buffer = bufnr, desc = "Rebuild Macros" })

--
-- move items
--
-- vim.cmd.RustLsp { 'moveItem',  'up' }
-- vim.cmd.RustLsp { 'moveItem',  'down' }

--
-- hover
--
-- vim.cmd.RustLsp { 'hover', 'actions' }
-- vim.cmd.RustLsp { 'hover', 'range' }

--
-- diagnostics
--
vim.keymap.set("n", "<leader>ae", function()
	vim.cmd.RustLsp("explainError")
end, { silent = true, buffer = bufnr, desc = "Explain Error" })

vim.keymap.set("n", "<leader>ad", function()
	vim.cmd.RustLsp("renderDiagnostic")
end, { silent = true, buffer = bufnr, desc = "Render Diagnostic" })

--
-- cargo
--
-- vim.cmd.RustLsp('openCargo')

--
-- docs
--
vim.keymap.set("n", "<leader>aD", function()
	vim.cmd.RustLsp("openDocs")
end, { silent = true, buffer = bufnr, desc = "Open Docs" })

--
-- modules
--
vim.keymap.set("n", "<leader>ap", function()
	vim.cmd.RustLsp("parentModule")
end, { silent = true, buffer = bufnr, desc = "Parent Module" })

--
-- workspace symbols
--
-- vim.cmd.RustLsp('workspaceSymbol')
-- -- or
-- vim.cmd.RustLsp {
--   'workspaceSymbol',
--   '<onlyTypes|allSymbols>' --[[ optional ]],
--   '<query>' --[[ optional ]],
--   bang = true --[[ optional ]]
-- }

vim.keymap.set("n", "<leader>aj", function()
	vim.cmd.RustLsp("joinLines")
end, { silent = true, buffer = bufnr, desc = "Join Lines" })

--
-- structurale search/replace
--
-- vim.cmd.RustLsp { 'ssr', '<query>' --[[ optional ]] }

--
-- graph
--
-- vim.cmd.RustLsp { 'crateGraph', '[backend]', '[output]' }

--
-- syntax tree
--
-- vim.cmd.RustLsp('syntaxTree')

--
-- flycheck
--
-- vim.cmd.RustLsp('flyCheck') -- defaults to 'run'
-- vim.cmd.RustLsp { 'flyCheck', 'run' }
-- vim.cmd.RustLsp { 'flyCheck', 'clear' }
-- vim.cmd.RustLsp { 'flyCheck', 'cancel' }

--
-- dev
--
-- vim.cmd.RustLsp { 'view', 'hir' }
-- vim.cmd.RustLsp { 'view', 'mir' }
-- vim.cmd.Rustc { 'unpretty', 'hir' }
-- vim.cmd.Rustc { 'unpretty', 'mir' }
