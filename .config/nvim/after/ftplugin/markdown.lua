local bufnr = vim.api.nvim_get_current_buf()

vim.keymap.set("n", "<leader>as", function()
	vim.cmd("Markview splitToggle")
end, { silent = true, buffer = bufnr, desc = "RenderMarkdown toggle" })

vim.keymap.set("n", "<leader>at", function()
	vim.cmd("Markview toggle")
end, { silent = true, buffer = bufnr, desc = "RenderMarkdown toggle" })
