return {
	name = "Current JavaScript",
	builder = function()
		return {
			cmd = { "node", vim.fn.expand("%:p") },
			cwd = vim.fn.getcwd(),
		}
	end,
	tags = { "files" },
	condition = {
		filetype = { "javascript" },
	},
}
