return {
	name = "Current Python",
	builder = function()
		return {
			cmd = { "python", vim.fn.expand("%:p") },
			cwd = vim.fn.getcwd(),
		}
	end,
	tags = { "files" },
	condition = {
		filetype = { "python" },
	},
}
