return {
	name = "Current Haskell",
	builder = function()
		return {
			cmd = { "runhaskell", vim.fn.expand("%:p") },
			cwd = vim.fn.getcwd(),
		}
	end,
	tags = { "files" },
	condition = {
		filetype = { "haskell", "lhaskell" },
	},
}
