return {
	name = "Current http",
	builder = function()
		return {
			cmd = { "httpyac", "send", "-a", vim.fn.expand("%:p") },
			cwd = vim.fn.getcwd(),
		}
	end,
	tags = { "files" },
	condition = {
		filetype = { "http" },
	},
}
