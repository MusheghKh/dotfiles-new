return {
	name = "Asciidoc preview html",
	builder = function()
		local preview_filename = "preview_adoc.html"
		local preview_path = vim.fn.getcwd() .. "/" .. preview_filename

		local full_cmd = {
			"sh",
			"-c",
			"asciidoctor"
				.. ""
				.. "-o"
				.. " "
				.. preview_path
				.. " "
				.. vim.fn.expand("%:p")
				.. ""
				.. "&&"
				.. " "
				.. "xdg-open"
				.. " "
				.. preview_path,
		}

		return {
			cmd = full_cmd,
			cwd = vim.fn.getcwd(),
		}
	end,
	tags = { "files" },
	condition = {
		filetype = { "adoc", "asciidoctor" },
	},
}
