return {
	name = "Current Shell",
	builder = function()
		return {
			cmd = { vim.fn.expand("%:p") },
			cwd = vim.fn.getcwd(),
		}
	end,
	tags = { "files" },
	condition = {
		filetype = { "sh" },
	},
}
