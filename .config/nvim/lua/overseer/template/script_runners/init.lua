return {
	"script_runners.perl",
	"script_runners.python",
	"script_runners.javascript",
	"script_runners.sh",
	"script_runners.haskell",
	"script_runners.http",
	"script_runners.markdown",
	"script_runners.adoc",
}
