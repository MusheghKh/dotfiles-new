return {
	name = "Current Perl",
	builder = function()
		return {
			cmd = { "perl", vim.fn.expand("%:p") },
			cwd = vim.fn.getcwd(),
		}
	end,
	tags = { "files" },
	condition = {
		filetype = { "perl" },
	},
}
