return {
	name = "Markdown preview html",
	builder = function()
		local preview_filename = "preview_markdown.html"
		local preview_path = vim.fn.getcwd() .. "/" .. preview_filename

		local full_cmd = {
			"sh",
			"-c",
			"pandoc"
				.. " -t html"
				.. " -o"
				.. " "
				.. preview_path
				.. " "
				.. vim.fn.expand("%:p")
				.. " "
				.. "&&"
				.. " "
				.. "xdg-open"
				.. " "
				.. preview_path,
		}

		return {
			cmd = full_cmd,
			cwd = vim.fn.getcwd(),
		}
	end,
	tags = { "files" },
	condition = {
		filetype = { "markdown" },
	},
}
