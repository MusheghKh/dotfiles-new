-- Generate gitignore files for various project types
return {
	"wintermute-cell/gitignore.nvim",
	lazy = true,
	cmd = {
		"Gitignore",
	},
	keys = {
		{ "<leader>gi", "<cmd>Gitignore<cr>", desc = "Generate gitignore" },
	},
}
