-- Remove old buffers automatically
return {
	"chrisgrieser/nvim-early-retirement",
	lazy = true,
	event = {
		"User SuperLazy",
	},
	opts = {
		-- if a buffer has been inactive for this many minutes, close it
		retirementAgeMins = 60,

		-- filetypes to ignore
		-- ignoredFiletypes = { "norg" },

		-- minimum number of open buffers for auto-closing to become active, e.g.,
		-- by setting this to 4, no auto-closing will take place when you have 3
		-- or less open buffers. Note that this plugin never closes the currently
		-- active buffer, so a number < 2 will effectively disable this setting.
		minimumBufferNum = 7,

		-- will ignore buffers with unsaved changes. If false, the buffers will
		-- automatically be written and then closed.
		ignoreUnsavedChangesBufs = true,

		-- Show notification on closing. Works with nvim-notify or noice.nvim
		notificationOnAutoClose = true,
	},
}
