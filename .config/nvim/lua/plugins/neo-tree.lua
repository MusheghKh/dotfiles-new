return {
	"nvim-neo-tree/neo-tree.nvim",
	branch = "v3.x",
	lazy = true,
	dependencies = {
		"nvim-lua/plenary.nvim",
		"nvim-tree/nvim-web-devicons", -- not strictly required, but recommended
		"MunifTanjim/nui.nvim",
		-- {"3rd/image.nvim", opts = {}}, -- Optional image support in preview window: See `# Preview Mode` for more information
	},
	cmd = {
		"Neotree",
	},
	keys = {
		{ "<leader>ft", "<cmd>Neotree reveal<cr>", desc = "Neotree" },
		{ "<leader>fT", "<cmd>Neotree filesystem current reveal<cr>", desc = "Neotree current" },
		{ "<leader>bL", "<cmd>Neotree buffers current<cr>", desc = "Neotree Buffers" },
	},
	opts = {
		window = {
			mappings = {
				["E"] = function()
					vim.cmd("Neotree focus filesystem left")
				end,
				["B"] = function()
					vim.cmd("Neotree focus buffers left")
				end,
				["D"] = "diff_files",
				["<tab>"] = function(state)
					state.commands["open"](state)
					vim.cmd("Neotree reveal")
				end,
				["/"] = "noop",
				["l"] = "noop",
				["f"] = "noop",
				["s"] = "noop",
				["S"] = "noop",
				["w"] = "noop",
				["<C-v>"] = "open_vsplit",
				["<C-s>"] = "open_split",
				["<C-l>"] = "focus_preview",
				["c"] = "none",
				["cc"] = "copy_path",
				["cf"] = "copy_filename",
				["cn"] = "copy_name",
			},
		},
		filesystem = {
			filtered_items = {
				hide_hidden = false, -- only works on Windows for hidden files/directories
				hide_dotfiles = false,
				hide_gitignored = false,
			},
			follow_current_file = {
				enabled = true,
				leave_dirs_open = false,
			},
			use_libuv_file_watcher=true,
			window = {
				mappings = {
					["o"] = "system_open",
					["ff"] = "picker_find",
					["fs"] = "picker_grep",
					["F"] = "filter_on_submit",
				},
			},
		},
		buffers = { follow_current_file = { enable = true } },
		commands = {
			system_open = function(state)
				local node = state.tree:get_node()
				local path = node:get_id()
				-- macOs: open file in default application in the background.
				-- vim.fn.jobstart({ "open", path }, { detach = true })
				-- Linux: open file in default application
				vim.fn.jobstart({ "xdg-open", path }, { detach = true })

				-- Windows: Without removing the file from the path, it opens in code.exe instead of explorer.exe
				local p
				local lastSlashIndex = path:match("^.+()\\[^\\]*$") -- Match the last slash and everything before it
				if lastSlashIndex then
					p = path:sub(1, lastSlashIndex - 1) -- Extract substring before the last slash
				else
					p = path -- If no slash found, return original path
				end
				vim.cmd("silent !start explorer " .. p)
			end,
			copy_path = function (state)
				local node = state.tree:get_node()
				vim.print(node.path)
				vim.fn.setreg("+", node.path)
			end,
			copy_filename = function (state)
				local node = state.tree:get_node()
				vim.print(node.name)
				vim.fn.setreg("+", node.name)
			end,
			copy_name = function (state)
				local node = state.tree:get_node()
				local name
				if string.find(node.name, "%.") then
					name = node.name:gsub('.' .. node.ext .. "$", "")
				else
					name = node.name
				end
				vim.print(name)
				vim.fn.setreg("+", name)
			end,
			picker_find = function(state)
				local node = state.tree:get_node()
				local path = node:get_id()
				local cwd = vim.fs.dirname(path)
				Snacks.picker.files({ cwd = cwd, hidden = true })
			end,
			picker_grep = function(state)
				local node = state.tree:get_node()
				local path = node:get_id()
				local cwd = vim.fs.dirname(path)
				Snacks.picker.grep({ cwd = cwd, hidden = true })
			end,
			diff_files = function(state)
				local node = state.tree:get_node()
				local log = require("neo-tree.log")
				state.clipboard = state.clipboard or {}
				if DIFF_NODE and DIFF_NODE ~= tostring(node.id) then
					local current_Diff = node.id
					require("neo-tree.utils").open_file(state, DIFF_NODE, open)
					vim.cmd("vert diffs " .. current_Diff)
					log.info("Diffing " .. DIFF_NAME .. " against " .. node.name)
					DIFF_NODE = nil
					current_Diff = nil
					state.clipboard = {}
					require("neo-tree.ui.renderer").redraw(state)
				else
					local existing = state.clipboard[node.id]
					if existing and existing.action == "diff" then
						state.clipboard[node.id] = nil
						DIFF_NODE = nil
						require("neo-tree.ui.renderer").redraw(state)
					else
						state.clipboard[node.id] = { action = "diff", node = node }
						DIFF_NAME = state.clipboard[node.id].node.name
						DIFF_NODE = tostring(state.clipboard[node.id].node.id)
						log.info("Diff source file " .. DIFF_NAME)
						require("neo-tree.ui.renderer").redraw(state)
					end
				end
			end,
		},
	},
}
