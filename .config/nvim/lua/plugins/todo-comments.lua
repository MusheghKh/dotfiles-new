return {
	"folke/todo-comments.nvim",
	dependencies = {
		"nvim-lua/plenary.nvim",
		"folke/trouble.nvim",
	},
	event = {
		"LspAttach",
	},
	lazy = true,
	keys = {
		{ "<leader>fC", "<cmd>TodoTrouble<cr>", desc = "TodoTrouble" },
		{ "<leader>fc", function ()
			Snacks.picker.todo_comments()
		end, desc = "TODO picker" },
	},
	opts = {
		highlight = {
			multiline = false,
		},
	},
}
