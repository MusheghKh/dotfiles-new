return {
	"iamcco/markdown-preview.nvim",
	build = "cd app && npm install",
	ft = { "markdown" },
	init = function ()
		vim.g.mkdp_filetypes = { "markdown" }
	end,
	-- config = function()
	--
	-- 	vim.cmd([[
	-- 	" Function to create buffer local mappings and add default compiler
	-- 	fun! MarkdownPreviewMappings()
	-- 			nnoremap <buffer> <leader>oh <Plug>MarkdownPreview
	-- 	endfun
	--
	-- 	" Call AsciidoctorMappings for all `*.md` files
	-- 	augroup markdown-preview
	-- 			au!
	-- 			au BufEnter *.md call MarkdownPreviewMappings()
	-- 	augroup END
	-- 	]])
	-- end,
}
