return {
	"nvim-pack/nvim-spectre",
	lazy = true,
	dependencies = {
		"nvim-lua/plenary.nvim",
	},
	-- keys = {
	-- 	{ "<leader>es", "<cmd>lua require('spectre').toggle()<CR>", desc = "Spectre (Search and replace)" },
	-- },
	cmd = {
		"Spectre",
	},
	config = true,
}
