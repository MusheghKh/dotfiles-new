return {
	"David-Kunz/gen.nvim",
	lazy = true,
	cmd = {
		"Gen",
	},
	keys = {
		{
			"<leader>cG",
			function()
				require("gen").select_model()
			end,
			mode = { "n" },
			desc = "Gen",
		},
		{ "<leader>cg", ":Gen<CR>", mode = { "n", "v" }, desc = "Gen" },
	},
	opts = {
		model = "zephyr", -- The default model to use.
		display_mode = "split", -- The display mode. Can be "float" or "split" or "horizontal-split".
		show_prompt = true, -- Shows the prompt submitted to Ollama.
		show_model = true, -- Displays which model you are using at the beginning of your chat session.
		no_auto_close = true, -- Never closes the window automatically.
	},
	config = function(_, opts)
		local gen = require("gen")
		gen.setup(opts)

		gen.prompts["Just a Question"] = {
			prompt = "$input?",
			replace = false,
		}
		gen.prompts["Elaborate_Text"] = {
			prompt = "Elaborate the following text:\n$text",
			replace = true,
		}
		gen.prompts["Fix_Code"] = {
			prompt = "Fix the following code. Only output the result in format ```$filetype\n...\n```:\n```$filetype\n$text\n```",
			replace = true,
			extract = "```$filetype\n(.-)```",
		}
	end,
}
