return {
	"williamboman/mason.nvim",
	lazy = true,
	cmd = "Mason",
	opts = {
		PATH = "append",
		ui = {
			icons = {
				package_installed = "✓",
				package_pending = "➜",
				package_uninstalled = "✗",
			},
			keymaps = {
				apply_language_filter = "F",
			},
		},
	},
}
