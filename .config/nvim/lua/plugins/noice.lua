return {
	"folke/noice.nvim",
	lazy = true,
	event = {
		"User SuperLazy",
	},
	dependencies = {
		-- if you lazy-load any plugin below, make sure to add proper `module="..."` entries
		"MunifTanjim/nui.nvim",
		-- "rcarriga/nvim-notify",
	},
	keys = {
		{ "<leader>el", "<cmd>Noice dismiss<cr><cmd>lua vim.opt.hlsearch = false<cr>", desc = "Dismiss Notifications" },
		{ "<leader>eL", "<cmd>Noice history<cr>", desc = "Noice history" },
	},
	opts = {
		cmdline = {
			enabled = true,
			view = "cmdline",
		},
		lsp = {
			-- override markdown rendering so that **cmp** and other plugins use **Treesitter**
			override = {
				["vim.lsp.util.convert_input_to_markdown_lines"] = true,
				["vim.lsp.util.stylize_markdown"] = true,
				["cmp.entry.get_documentation"] = true,
			},
		},
		-- you can enable a preset for easier configuration
		presets = {
			bottom_search = true, -- use a classic bottom cmdline for search
			command_palette = false, -- position the cmdline and popupmenu together
			long_message_to_split = true, -- long messages will be sent to a split
			inc_rename = false, -- enables an input dialog for inc-rename.nvim
			lsp_doc_border = true, -- add a border to hover docs and signature help
		},
		views = {
			cmdline_popup = {
				position = {
					row = "50%",
					col = "50%",
				},
				size = {
					width = 60,
					height = "auto",
				},
			},
		},
		routes = {
			-- Do not show file written messages
			-- {
			-- 	filter = {
			-- 		event = "msg_show",
			-- 		find = "B written",
			-- 	},
			-- 	opts = {
			-- 		skip = true,
			-- 	},
			-- },

			-- show recording @ macro messages
			{
				view = "notify",
				filter = { event = "msg_showmode" },
			},

			-- Do not show search stuck messages
			-- {
			-- 	filter = {
			-- 		event = "msg_show",
			-- 		find = "E384: search hit TOP without match for:",
			-- 	},
			-- 	opts = {
			-- 		skip = true,
			-- 	},
			-- },
			-- {
			-- 	filter = {
			-- 		event = "msg_show",
			-- 		find = "E385: search hit BOTTOM without match for:",
			-- 	},
			-- 	opts = {
			-- 		skip = true,
			-- 	},
			-- },
		},
	},
}
