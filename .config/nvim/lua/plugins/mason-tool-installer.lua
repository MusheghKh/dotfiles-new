return {
	"WhoIsSethDaniel/mason-tool-installer.nvim",
	dependencies = {
		"williamboman/mason.nvim",
	},
	cmd = {
		"MasonToolsInstall",
		"MasonToolsUpdate",
		"MasonToolsClean",
	},
	lazy = true,
	opts = {
		-- a list of all tools you want to ensure are installed
		ensure_installed = {
			-- LSP
			"json-lsp",
			"pyright",
			"lua-language-server",
			"typescript-language-server",
			"solargraph",
			"gopls",
			"clangd",
			"arduino-language-server",
			-- "rust-analyzer",
			"perlnavigator",
			"sqlls",
			"awk-language-server",
			"jq-lsp",
			"powershell-editor-services",
			"cmake-language-server",
			"terraform-ls",
			"ansible-language-server",
			"puppet-editor-services",
			"dockerfile-language-server",
			"docker-compose-language-service",
			"css-lsp",
			"html-lsp",
			"yaml-language-server",
			"texlab", -- Latex
			"rnix-lsp", -- Nix
			-- "buf-language-server",
			"bash-language-server",
			"pbls", -- Protobuf

			-- DAP
			"debugpy",
			"bash-debug-adapter",
			"delve",
			"cpptools",
			"codelldb",
			"js-debug-adapter",
			"haskell-debug-adapter",

			-- Lint
			"hadolint",
			"yamllint",
			"pylint",
			"ansible-lint",
			"shellcheck",
			"cmakelint",
			"rubocop",
			"cpplint",
			"sqlfluff",
			"stylelint",
			"golangci-lint",
			"eslint_d",
			"codespell",
			"typos",
			"markdownlint",
			"checkmake",
			"editorconfig-checker",
			"npm-groovy-lint",
			"systemdlint",
			"tflint",
			"tfsec",
			"vint",

			-- Format
			"prettier",
			"stylua",
			"clang-format",
			"latexindent",
			"black",
			"isort",
			"fourmolu",
			"sql-formatter",
			"shfmt",
			"gofumpt",
			"goimports",
			"goimports-reviser",
			"fixjson",

			-- Multi
			"buf",
			"cmakelang",
		},
		-- if set to true this will check each tool for updates. If updates
		-- are available the tool will be updated. This setting does not
		-- affect :MasonToolsUpdate or :MasonToolsInstall.
		-- Default: false
		auto_update = false,
		-- automatically install / update on startup. If set to false nothing
		-- will happen on startup. You can use :MasonToolsInstall or
		-- :MasonToolsUpdate to install tools and check for updates.
		-- Default: true
		run_on_start = false,
	},
}
