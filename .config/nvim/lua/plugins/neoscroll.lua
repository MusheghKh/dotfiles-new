return {
	"karb94/neoscroll.nvim",
	lazy = true,
	enabled = true,
	event = {
		-- "BufReadPre",
		-- "BufWinEnter",
		"CursorMoved",
	},
	config = function()
		local neoscroll = require("neoscroll")
		neoscroll.setup({
			mappings = { -- Keys to be mapped to their corresponding default scrolling animation
				"<C-u>",
				"<C-d>",
				-- '<C-b>', '<C-f>',
				"<C-y>",
				"<C-e>",
				-- 'zt', 'zz', 'zb',
			},
			hide_cursor = true,
			easing = 'linear',
			-- easing = "quadratic",
		})
	end,
}
