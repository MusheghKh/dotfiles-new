return {
	"neovim/nvim-lspconfig",
	lazy = true,
	dependencies = {
		"folke/neoconf.nvim",
	},
}
