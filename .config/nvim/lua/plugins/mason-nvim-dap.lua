return {
	"jay-babu/mason-nvim-dap.nvim",
	dependencies = {
		"williamboman/mason.nvim",
		"mfussenegger/nvim-dap",
	},
	lazy = true,
	event = {
		"LspAttach",
	},
	keys = {
		{
			"<leader>ds",
			function()
				require("dap").terminate({}, { terminateDebuggee = true }, function()
					vim.cmd([[ DapVirtualTextForceRefresh ]])
				end)
			end,
			desc = "[DAP] Terminate",
		},
		{ "<leader>dC", "<cmd>lua require('dap.ext.vscode').load_launchjs()<cr>", desc = "[DAP] Load vscode json" },

		{ "dl", "<cmd>lua require'dap'.continue()<cr>", desc = "[DAP] DAP Continue" },
		{ "<leader>dl", "<cmd>lua require'dap'.run_last()<cr>", desc = "[DAP] Run last" },
		{ "dp", "<cmd>lua require'dap'.toggle_breakpoint()<cr>", desc = "[DAP] Breakpoint" },
		{
			"dP",
			"<cmd>lua require'dap'.toggle_breakpoint(vim.fn.input('Breakpoint condition: '))<cr>",
			desc = "[DAP] Breakpoint with condition",
		},
		{ "do", "<cmd>lua require'dap'.step_over()<cr>", desc = "[DAP] Step Over" },
		{ "dh", "<cmd>lua require'dap'.step_into()<cr>", desc = "[DAP] Step Into" },
		{ "dH", "<cmd>lua require'dap'.step_out()<cr>", desc = "[DAP] Step Out" },
	},
	opts = {
		automatic_setup = true,
		handlers = {
			function(config)
				-- all sources with no handler get passed here

				-- Keep original functionality of `automatic_setup = true`
				require("mason-nvim-dap").default_setup(config)
			end,
			-- for dap vscode js
			-- NOTE: js is unconfigured
			-- js = function(config)
			-- local dap = require("dap")
			-- require("dap-vscode-js").setup({
			-- 	-- node_path = "node", -- Path of node executable. Defaults to $NODE_PATH, and then "node"
			-- 	-- debugger_path = "(runtimedir)/site/pack/packer/opt/vscode-js-debug", -- Path to vscode-js-debug installation.
			-- 	debugger_cmd = { "js-debug-adapter" }, -- Command to use to launch the debug server. Takes precedence over `node_path` and `debugger_path`.
			-- 	adapters = { "pwa-node", "pwa-chrome", "pwa-msedge", "node-terminal", "pwa-extensionHost" }, -- which adapters to register in nvim-dap
			-- 	-- log_file_path = "(stdpath cache)/dap_vscode_js.log" -- Path for file logging
			-- 	-- log_file_level = false -- Logging level for output to file. Set to false to disable file logging.
			-- 	-- log_console_level = vim.log.levels.ERROR -- Logging level for output to console. Set to false to disable console output.
			-- })

			-- dap.adapters["pwa-node"] = {
			-- 	type = "server",
			-- 	host = "localhost",
			-- 	port = "${port}",
			-- 	executable = {
			-- 		command = "js-debug-adapter",
			-- 		-- 💀 Make sure to update this path to point to your installation
			-- 		args = { "${port}" },
			-- 	},
			-- }
			-- dap.configurations["typescript"] = {}
			-- dap.configurations["javascript"] = {}
			-- end,
		},
	},
	config = function(plugin, opts)
		local mason_nvim_dap = require("mason-nvim-dap")

		mason_nvim_dap.setup(opts)

		-- NOTE: Haskell is unconfigured
		-- -- Haskell
		-- dap.adapters.haskell = {
		-- 	type = "executable",
		-- 	command = "haskell-debug-adapter",
		-- 	-- args = { "--hackage-version=0.0.38.0" },
		-- }
		-- dap.configurations.haskell = {
		-- 	{
		-- 		type = "haskell",
		-- 		request = "launch",
		-- 		name = "Debug",
		-- 		workspace = "${workspaceFolder}",
		-- 		startup = "${file}",
		-- 		stopOnEntry = true,
		-- 		logFile = vim.fn.stdpath("data") .. "/haskell-dap.log",
		-- 		logLevel = "WARNING",
		-- 		ghciEnv = vim.empty_dict(),
		-- 		ghciPrompt = "λ: ",
		-- 		-- Adjust the prompt to the prompt you see when you invoke the stack ghci command below
		-- 		-- ghciInitialPrompt = "λ: ",
		-- 		ghciInitialPrompt = "λ> ",
		-- 		ghciCmd = "stack ghci --test --no-load --no-build --main-is TARGET --ghci-options -fprint-evld-with-show",
		-- 	},
		-- }
	end,
}
