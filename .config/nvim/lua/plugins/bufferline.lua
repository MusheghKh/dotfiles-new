return {
	"akinsho/bufferline.nvim",
	version = "*",
	dependencies = {
		{ "nvim-tree/nvim-web-devicons" },
	},
	lazy = true,
	event = {
		"User SuperLazy",
	},
	keys = {
		{ "<C-h>", "<cmd>BufferLineCyclePrev<cr>", desc = "Buffer Previous" },
		{ "<C-l>", "<cmd>BufferLineCycleNext<cr>", desc = "Buffer Next" },
		{ "<C-j>", "<cmd>BufferLineMovePrev<cr>", desc = "Buffer Move Previous" },
		{ "<C-k>", "<cmd>BufferLineMoveNext<cr>", desc = "Buffer Move Next" },

		{ "<C-h>", mode = "i", "<cmd>BufferLineCyclePrev<cr>", desc = "Buffer Previous" },
		{ "<C-l>", mode = "i", "<cmd>BufferLineCycleNext<cr>", desc = "Buffer Next" },
		{ "<C-j>", mode = "i", "<cmd>BufferLineMovePrev<cr>", desc = "Buffer Move Previous" },
		{ "<C-k>", mode = "i", "<cmd>BufferLineMoveNext<cr>", desc = "Buffer Move Next" },

		{ "<leader>b1", "<cmd>BufferLineGoToBuffer 1<cr>", desc = "Go 1" },
		{ "<leader>b2", "<cmd>BufferLineGoToBuffer 2<cr>", desc = "Go 2" },
		{ "<leader>b3", "<cmd>BufferLineGoToBuffer 3<cr>", desc = "Go 3" },
		{ "<leader>b4", "<cmd>BufferLineGoToBuffer 4<cr>", desc = "Go 4" },
		{ "<leader>b5", "<cmd>BufferLineGoToBuffer 5<cr>", desc = "Go 5" },
		{ "<leader>b6", "<cmd>BufferLineGoToBuffer 6<cr>", desc = "Go 6" },
		{ "<leader>b7", "<cmd>BufferLineGoToBuffer 7<cr>", desc = "Go 7" },
		{ "<leader>b8", "<cmd>BufferLineGoToBuffer 8<cr>", desc = "Go 8" },
		{ "<leader>b9", "<cmd>BufferLineGoToBuffer 9<cr>", desc = "Go 9" },
		{ "<leader>b0", "<cmd>BufferLineGoToBuffer -1<cr>", desc = "Go Last" },

		{ "<leader>bm", "<cmd>BufferLineTogglePin<cr>", desc = "Pin" },
		-- { "<leader>bk", "<cmd>Bdelete<cr>", desc = "Close" },
		-- {
		-- 	"<leader>bK",
		-- 	"<cmd>BufferLineCloseLeft<cr><cmd>BufferLineCloseRight<cr><cmd>Bdelete<cr>",
		-- 	desc = "Close all",
		-- },

		{ "<leader>bch", "<cmd>BufferLineCloseLeft<cr>", desc = "Close Left" },
		{ "<leader>bcl", "<cmd>BufferLineCloseRight<cr>", desc = "Close Right" },

		-- {
		-- 	"<leader>bcc",
		-- 	"<cmd>BufferLineCloseLeft<cr><cmd>BufferLineCloseRight<cr>",
		-- 	desc = "Close all but current",
		-- },
		{ "<leader>bb", "<cmd>BufferLinePick<cr>", desc = "Pick" },
	},
	opts = {
		options = {
			numbers = "both", -- | "ordinal" | "buffer_id" | "both" | function({ ordinal, id, lower, raise }): string,
			close_command = "lua Snacks.bufdelete.delete(%d)", -- can be a string | function, see "Mouse actions"
			right_mouse_command = "lua Snacks.bufdelete.delete(%d)", -- can be a string | function, see "Mouse actions"
			left_mouse_command = "buffer %d", -- can be a string | function, see "Mouse actions"
			middle_mouse_command = nil, -- can be a string | function, see "Mouse actions"
			diagnostics = "nvim_lsp", -- | "nvim_lsp" | "coc",
			diagnostics_indicator = function(count, level)
				local icon
				if level:match("error") then
					icon = ""
				elseif level:match("warn") then
					icon = ""
				elseif level:match("info") then
					icon = ""
				else
					icon = ""
				end
				return icon .. " " .. count
			end,
			diagnostics_update_in_insert = false,
			offsets = {
				{ filetype = "NvimTree", text = "File Tree", padding = 1 },
				{ filetype = "neo-tree", text = "File Tree", padding = 1 },
				{ filetype = "OverseerList", text = "Tasks", padding = 1 },
				{ filetype = "dapui_scopes", text = "DAP", padding = 1 },
			},
			show_buffer_icons = true,
			show_buffer_close_icons = false,
			show_close_icon = false,
			show_tab_indicators = true,
			separator_style = "thin",
			-- separator_style = "slant" | "thick" | "thin" | { 'any', 'any' },
		},
	},
}
