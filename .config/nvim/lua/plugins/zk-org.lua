return {
	"zk-org/zk-nvim",
	lazy = true,
	cmd = {
		"ZkIndex",
		"ZkNew",
		"ZkNewFromTitleSelection",
		"ZkNewFromContentSelection",
		"ZkCd",
		"ZkNotes",
		"ZkBacklinks",
		"ZkLinks",
		"ZkInsertLink",
		"ZkInsertLinkAtSelection",
		"ZkMatch",
		"ZkTags",
	},
	keys = {
		{ "<leader>zi", "<Cmd>ZkIndex<CR>", mode = "n", desc = "Index Notebook" },
		{
			"<leader>zn",
			function()
				local title = vim.fn.input("Title: ")
				if title ~= "" then
					vim.cmd("ZkNew { title = '" .. title .. "' }")
				else
					vim.notify("Title can not be empty. Ignoring...", vim.log.levels.WARN)
				end
			end,
			mode = "n",
			desc = "New Note",
		},
		{
			"<leader>zj",
			function()
				vim.cmd("ZkNew { title = '" .. os.date("%Y-%m-%d") .. "', dir = 'journal" .. "'}")
			end,
			mode = "n",
			desc = "New Journal Note",
		},
		{
			"<leader>zN",
			function()
				local title = vim.fn.input("Title: ")
				if title ~= "" then
					local dir = vim.fn.input("Directory: ")
					if dir ~= "" then
						vim.cmd("ZkNew { title = '" .. title .. "', dir = '" .. dir .. "' }")
					else
						vim.notify("Directory can not be empty. Ignoring...", vim.log.levels.WARN)
					end
				else
					vim.notify("Title can not be empty. Ignoring...", vim.log.levels.WARN)
				end
			end,
			mode = "n",
			desc = "New Journal Note",
		},
		{ "<leader>zo", "<Cmd>ZkNotes { sort = { 'modified' } }<CR>", mode = "n", desc = "List Notes" },
		{ "<leader>zb", "<Cmd>ZkBuffers<CR>", mode = "n", desc = "List ZK Buffers" },
		{ "<leader>zt", "<Cmd>ZkTags<CR>", mode = "n", desc = "List Tags" },
		{ "<leader>zf", ":'<,'>ZkMatch<CR>", mode = "v", desc = "Match" },
		{ "<leader>zl", "<Cmd>ZkInsertLink<CR>", mode = "n", desc = "Insert Link" },
		{ "<leader>zL", "<Cmd>ZkLinks<CR>", mode = "n", desc = "Links to current" },
		{
			"<leader>zd",
			function()
				vim.cmd("cd ~/zk")
				vim.notify("Welcome to zk!!!")
				-- vim.cmd("Oil")
			end,
			mode = "n",
			desc = "Change directory to ~/zk",
		},
	},
	opts = {
		picker = "snacks_picker",
	},
	config = function(_, opts)
		require("zk").setup(opts)
	end,
}
