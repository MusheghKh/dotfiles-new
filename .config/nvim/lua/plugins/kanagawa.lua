return {
	"rebelot/kanagawa.nvim",
	dependencies = {
		"xiyaowong/transparent.nvim",
	},
	lazy = true,
	-- lazy = false,
	-- priority = 1000,
	event = {
		"CmdlineEnter",
	},
	opts = {
    transparent = vim.g.transparent_enabled,
  }
}
