--- Show Current Working Directory
---@return string
local function show_cwd()
	local cwd = vim.fn.getcwd()
	local home = vim.fn.getenv("HOME")
	if vim.startswith(cwd, home) then
		cwd = string.gsub(cwd, vim.fn.getenv("HOME"), "~")
	end
	return cwd
end

--- Get current buffer path relative to CWD
---@param full_path string
---@return string
local function parse_current_buf_path(full_path)
	local cwd = vim.fn.getcwd()
	if vim.startswith(full_path, cwd) then
		return string.sub(full_path, cwd:len() + 2, full_path:len())
	end
	return full_path
end

--- Show current buffer path relative to CWD
---@return string
local function show_current_buf_path()
	local full_path = vim.api.nvim_buf_get_name(0)
	return parse_current_buf_path(full_path)
end

--- Show lspsaga lsp symbols
---@return string
local function showLspSagaBar()
	local bar = require("lspsaga.symbol.winbar").get_bar()
	if bar == nil then
		return "..."
	end
	return bar
end

-- status line
return {
	"nvim-lualine/lualine.nvim",
	lazy = true,
	event = {
		"User SuperLazy",
	},
	dependencies = {
		"nvim-tree/nvim-web-devicons",
	},
	opts = {
		options = {
			icons_enabled = true,
			theme = "auto",
			-- component_separators = { left = "", right = "" },
			component_separators = "|",
			-- section_separators = { left = "", right = "" },
			-- section_separators = { left = "", right = "" },
			section_separators = {},
			disabled_filetypes = {
				statusline = {},
				winbar = {},
			},
			ignore_focus = {},
			always_divide_middle = true,
			globalstatus = true,
			refresh = {
				statusline = 1000,
				tabline = 1000,
				winbar = 1000,
			},
		},
		sections = {
			lualine_a = { "mode" },
			-- lualine_b = { "branch", "diff", "diagnostics" },
			lualine_b = {
				show_cwd,
			},
			lualine_c = {
				{
					show_current_buf_path,
				},
			},
			lualine_x = {
				{ "encoding" },
				{ "fileformat" },
				{ "filetype" },
			},
			lualine_y = { "progress" },
			lualine_z = { "location" },
		},
		inactive_sections = {
			lualine_a = {},
			lualine_b = {
				show_cwd,
			},
			lualine_c = {},
			lualine_x = { "location" },
			lualine_y = {},
			lualine_z = {},
		},
		tabline = {},
		winbar = {
			lualine_a = {},
			lualine_b = {},
			lualine_c = {
				-- showLspSagaBar,
			},
			lualine_x = {},
			lualine_y = {},
			lualine_z = {},
		},
		inactive_winbar = {
			lualine_a = {},
			lualine_b = {},
			lualine_c = {
				-- function()
				-- 	return "..."
				-- end,
			},
			lualine_x = {},
			lualine_y = {},
			lualine_z = {},
		},
		extensions = {},
	},
	config = function(_, opts)
		require("lualine").setup(opts)
		require("lualine").hide({ place = { "winbar" }, unhide = false })
	end,
}
