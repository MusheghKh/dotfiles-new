return {
	"catppuccin/nvim",
	name = "catppuccin",
	dependencies = {
		"xiyaowong/transparent.nvim",
	},
	-- lazy = true,
	lazy = false,
	priority = 1000,
	event = {
		"CmdlineEnter",
	},
	opts = {
		flavour = "macchiato", -- latte, frappe, macchiato, mocha
		background = { -- :h background
			light = "latte",
			dark = "macchiato",
		},
		transparent_background = vim.g.transparent_enabled,
		term_colors = true,
		dim_inactive = {
			enabled = false,
			shade = "dark",
			percentage = 0.75,
		},
		no_italic = false, -- Force no italic
		no_bold = false, -- Force no bold
		no_underline = false, -- Force no underline
		styles = {
			comments = { "italic" },
			conditionals = { "italic" },
			loops = {},
			functions = {},
			keywords = {},
			strings = {},
			variables = {},
			numbers = {},
			booleans = {},
			properties = {},
			types = {},
			operators = {},
		},
		color_overrides = {},
		custom_highlights = {},
		integrations = {
			cmp = true,
			gitsigns = true,
			mini = false,
			noice = true,
			flash = true,
			snacks = true,
			lsp_trouble = true,
			diffview = true,
			neotest = true,
			nvim_surround = true,
			overseer = true,
			which_key = true,
			-- For more plugins integrations please scroll down (https://github.com/catppuccin/nvim#integrations)
		},
	},
}
