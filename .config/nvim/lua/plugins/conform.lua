return {
	"stevearc/conform.nvim",
	dependencies = {
		"williamboman/mason.nvim",
	},
	lazy = true,
	keys = {
		{
			"<leader>cf",
			function()
				require("conform").format({ async = true, lsp_fallback = true })
			end,
			mode = "",
			desc = "Conform",
		},
	},
	opts = {
		formatters_by_ft = {
			javascript = { "prettier" },
			javascriptreact = { "prettier" },
			typescript = { "prettier" },
			typescriptreact = { "prettier" },

			vue = { "prettier" },
			css = { "prettier" },
			scss = { "prettier" },
			less = { "prettier" },
			html = { "prettier" },
			json = { "prettier", "fixjson" },
			jsonc = { "prettier" },

			markdown = { "prettier" },
			["markdown.mdx"] = { "prettier" },

			graphql = { "prettier" },
			handlebars = { "prettier" },

			yaml = { "prettier" },
			["yaml.ansible"] = { "prettier", "ansible-lint" },
			["yaml.docker-compose"] = { "prettier" },

			c = { "clang_format" },
			cpp = { "clang_format" },
			go = { "gofumpt", "goimports", "goimports-reviser" },
			rust = { "rustfmt" },
			haskell = { "fourmolu" },
			python = { "isort", "black" },
			lua = { "stylua" },
			ruby = { "rubocop" },
			bash = { "shfmt" },
			sh = { "shfmt" },
			perl = { "perltidy" },
			awk = { "awk" },

			terraform = { "terraform_fmt" },
			hcl = { "packer_fmt" },

			proto = { "buf" },
			sql = { "sql_formatter " },
			tex = { "latexindent" },
			cmake = { "cmake_format" },

			-- Use the "_" filetype to run formatters on filetypes that don't
			-- have other formatters configured.
			["_"] = { "trim_whitespace" },
		},
	},
}
