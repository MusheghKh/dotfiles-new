local user_func = require("user.func")
return {
	"stevearc/overseer.nvim",
	dependencies = {
		-- "nvim-telescope/telescope.nvim",
		-- "rcarriga/nvim-notify",
		-- "jay-babu/mason-nvim-dap.nvim",
	},
	lazy = true,
	event = {
		"LspAttach",
	},
	cmd = {
		"OverseerToggle",
		"OverseerQuickAction",
		"OverseerBuild",
		"OverseerRun",
		"OverseerInfo",
		"OverseerOpen",
		"OverseerClose",
		"OverseerRunCmd",
		"OverseerClearCache",
		"OverseerLoadBundle",
		"OverseerSaveBundle",
		"OverseerTaskAction",
		"OverseerDeleteBundle",
	},
	keys = {
		{
			"<leader>rr",
			function()
        require("overseer").load_task_bundle()
			end,
			desc = "Run",
		},
		{
			"<leader>rt",
			function()
				require("overseer").run_template({
					prompt = "allow",
				}, function(task, err)
					if task then
						require("overseer").open({ enter = false })
					end
				end)
			end,
			desc = "Run",
		},
		{
			"<leader>rf",
			function()
				require("overseer").run_template({
					prompt = "allow",
					tags = {
						"files",
					},
				}, function(task, err)
					if task then
						require("overseer").open({ enter = false })
					end
				end)
			end,
			desc = "Run by Tag",
		},
		{ "<leader>rb", "<cmd>OverseerBuild<cr>", desc = "Build" },
		{
			"<leader>ru",
			function()
				require("overseer").toggle({ enter = false })
			end,
			desc = "Task Panel",
		},
		{ "<leader>rq", "<cmd>OverseerQuickAction<cr>", desc = "Quck Action" },
		{ "<leader>rl", "<cmd>OverseerQuickAction restart<cr>", desc = "Last Restart" },
		{ "<leader>rs", "<cmd>OverseerQuickAction open hsplit<cr>", desc = "Last Split" },
		{ "<leader>rv", "<cmd>OverseerQuickAction open vsplit<cr>", desc = "Last Split" },
		{ "<leader>ro", "<cmd>OverseerQuickAction open<cr>", desc = "Last Split" },
	},
	opts = {
		-- FIX: check if this working
		strategy = "terminal",
		task_list = {
			max_height = { 20, 0.2 },
			min_height = 15,
			direction = "bottom",
		},
	},
	config = function(plugin, opts)
		local overseer = require("overseer")

		overseer.setup(opts)

		overseer.load_template("script_runners")
	end,
}
