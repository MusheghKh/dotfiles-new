return {
	"williamboman/mason-lspconfig.nvim",
	dependencies = {
		"williamboman/mason.nvim",
		"neovim/nvim-lspconfig",
		"hrsh7th/cmp-nvim-lsp",
	},
	lazy = true,
	event = {
		"FileType",
	},
	opts = {},
	config = function(plugin, opts)
		local user_func = require("user.func")
		local lspconfig = require("lspconfig")
		local cmp_nvim_lsp = require("cmp_nvim_lsp")
		local mason_lspconfig = require("mason-lspconfig")

		local function lspSymbol(name, icon, hl)
			vim.fn.sign_define("DiagnosticSign" .. name, { text = icon, numhl = "DiagnosticDefault" .. name, texthl = hl })
		end
		lspSymbol("Error", "", "TroubleSignError")
		lspSymbol("Warning", "", "TroubleSignWarning")
		lspSymbol("Warn", "", "TroubleSignWarning")
		lspSymbol("Info", "", "TroubleSignInformation")
		lspSymbol("Information", "", "TroubleSignInformation")
		lspSymbol("Hint", "", "TroubleSignHint")

		vim.api.nvim_create_user_command("LspToggleInlayHints", function()
			local enabled = vim.lsp.inlay_hint.is_enabled()
			vim.lsp.inlay_hint.enable(not enabled)
			vim.print(enabled and "Inlay Hints Disabled" or "Inlay Hints Enabled")
		end, {})

		local on_attach = function(client, bufnr)
			if client.server_capabilities.inlayHintProvider then
				vim.lsp.inlay_hint.enable(true, { bufnr })
			end
		end

		local function get_root_dir(root_files)
			return function(fname)
				return lspconfig.util.root_pattern(unpack(root_files))(fname)
					or vim.fs.dirname(vim.fs.find(".git", { path = fname, upward = true })[1])
					or vim.fs.dirname(fname)
			end
		end

		local capabilities = cmp_nvim_lsp.default_capabilities(vim.lsp.protocol.make_client_capabilities())

		--- this is a workaround to the limitations of copilot language server
		capabilities = vim.tbl_deep_extend("force", capabilities, {
			offsetEncoding = { "utf-16" },
			general = {
				positionEncodings = { "utf-16" },
			},
		})

		local options = {
			on_attach = on_attach,
			capabilities = capabilities,
			root_dir = get_root_dir({}),
		}

		mason_lspconfig.setup(opts)
		mason_lspconfig.setup_handlers({
			-- The first entry (without a key) will be the default handler
			-- and will be called for each installed server that doesn't have
			-- a dedicated handler.
			function(server_name) -- default handler (optional)
				lspconfig[server_name].setup(options)
			end,
			["lua_ls"] = function()
				local lua_ls_options = user_func.shallow_copy(options)
				lua_ls_options.settings = {
					Lua = {
						hint = {
							enable = true,
						},
						runtime = {
							-- Tell the language server which version of Lua you're using (most likely LuaJIT in the case of Neovim)
							version = "LuaJIT",
						},
						diagnostics = {
							-- Get the language server to recognize the `vim` global
							globals = { "vim" },
						},
						workspace = {
							-- Make the server aware of Neovim runtime files
							library = vim.api.nvim_get_runtime_file("", true),
							checkThirdParty = false,
						},
					},
				}
				lspconfig["lua_ls"].setup(lua_ls_options)
			end,
			["ansiblels"] = function()
				local ansible_lsp_options = user_func.shallow_copy(options)
				ansible_lsp_options.settings = {
					ansible = {
						validation = {
							lint = {
								enabled = false,
							},
						},
					},
				}
				lspconfig["ansiblels"].setup(ansible_lsp_options)
			end,
			["yamlls"] = function()
				local yamlls_options = user_func.shallow_copy(options)
				yamlls_options.settings = {
					yaml = {
						keyOrdering = false,
						schemas = {
							kubernetes = "*k8s/*.yaml",
							["http://json.schemastore.org/github-workflow"] = ".github/workflows/*",
							["http://json.schemastore.org/github-action"] = ".github/action.{yml,yaml}",
							["http://json.schemastore.org/ansible-stable-2.9"] = "roles/tasks/*.{yml,yaml}",
							["http://json.schemastore.org/prettierrc"] = ".prettierrc.{yml,yaml}",
							["http://json.schemastore.org/kustomization"] = "kustomization.{yml,yaml}",
							["http://json.schemastore.org/ansible-playbook"] = "*play*.{yml,yaml}",
							["http://json.schemastore.org/chart"] = "Chart.{yml,yaml}",
							["https://json.schemastore.org/dependabot-v2"] = ".github/dependabot.{yml,yaml}",
							["https://json.schemastore.org/gitlab-ci"] = "*gitlab-ci*.{yml,yaml}",
						},
					},
					redhat = {
						telemetry = {
							enabled = false,
						},
					},
				}
				lspconfig["yamlls"].setup(yamlls_options)
			end,
			["ts_ls"] = function()
				local ts_ls_options = user_func.shallow_copy(options)
				ts_ls_options.init_options = {
					preferences = {
						includeInlayParameterNameHints = "all",
						includeInlayParameterNameHintsWhenArgumentMatchesName = true,
						includeInlayFunctionParameterTypeHints = true,
						includeInlayVariableTypeHints = true,
						includeInlayPropertyDeclarationTypeHints = true,
						includeInlayFunctionLikeReturnTypeHints = true,
						includeInlayEnumMemberValueHints = true,
						importModuleSpecifierPreference = "non-relative",
					},
				}
				lspconfig["ts_ls"].setup(ts_ls_options)
			end,
			["gopls"] = function()
				local gopls_options = user_func.shallow_copy(options)
				gopls_options.settings = {
					gopls = {
						gofumpt = true,
						staticcheck = true,
						analyses = {
							unusedparams = true,
							unusedvariable = true,
							unusedwrite = true,
						},
						hints = {
							assignVariableTypes = true,
							compositeLiteralFields = true,
							constantValues = true,
							functionTypeParameters = true,
							parameterNames = true,
							rangeVariableTypes = true,
						},
					},
				}
				lspconfig["gopls"].setup(gopls_options)
			end,
		})

		-- Not supported by Mason
		local qmlls_options = user_func.shallow_copy(options)
		qmlls_options.filetypes = {
			"qmljs",
			"qml",
		}
		lspconfig["qmlls"].setup(qmlls_options)

		-- Installed with ghcup
		local hls_options = user_func.shallow_copy(options)
		hls_options.root_dir = function(fname)
			return lspconfig.util.root_pattern("hie.yaml", "stack.yaml", "cabal.project")(fname)
				or lspconfig.util.root_pattern("*.cabal", "package.yaml")(fname)
				or lspconfig.util.find_git_ancestor(fname)
				or lspconfig.util.path.dirname(fname)
		end
		lspconfig["hls"].setup(hls_options)
	end,
}
