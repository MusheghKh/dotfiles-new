return {
	"folke/edgy.nvim",
	event = "VeryLazy",
	keys = {
		{
			"<leader>ep",
			function()
				require("edgy").toggle()
			end,
			desc = "Edgy Toggle",
		},
    -- stylua: ignore
    { "<leader>eP", function() require("edgy").select() end, desc = "Edgy Select Window" },
	},
	init = function()
		-- views can only be fully collapsed with the global statusline
		vim.opt.laststatus = 3
		-- Default splitting will cause your main splits to jump when opening an edgebar.
		-- To prevent this, set `splitkeep` to either `screen` or `topline`.
		vim.opt.splitkeep = "screen"
	end,
	opts = function()
		local opts = {
			animate = {
				enabled = false,
			},
			bottom = {
				{
					ft = "noice",
					size = { height = 0.4 },
					filter = function(buf, win)
						return vim.api.nvim_win_get_config(win).relative == ""
					end,
				},
				"Trouble",
				{ ft = "qf", title = "QuickFix" },
				{
					ft = "help",
					size = { height = 20 },
					-- don't open help files in edgy that we're editing
					filter = function(buf)
						return vim.bo[buf].buftype == "help"
					end,
				},
				{ title = "Neotest Output", ft = "neotest-output-panel", size = { height = 15 } },
				-- { title = "Overseer", ft = "OverseerList", size = { height = 2 } },
				"dap-repl",
				"dapui_console",
			},
			left = {
				{ title = "Neotest Summary", ft = "neotest-summary" },
				-- Neo-tree filesystem always takes half the screen height
				{
					title = "Neo-Tree",
					ft = "neo-tree",
					filter = function(buf)
						return vim.b[buf].neo_tree_source == "filesystem"
					end,
					size = { width = 0.2 },
					open = "Neotree position=left filesystem show",
				},
				-- any other neo-tree windows
				"neo-tree",

				"dapui_scopes",
				"dapui_breakpoints",
				"dapui_stacks",
				"dapui_watches",
			},
			right = {
				{ title = "Spectre", ft = "spectre_panel", size = { width = 0.4 } },
				-- { title = "Grug Far", ft = "grug-far", size = { width = 0.4 } },
			},
			keys = {
				-- increase width
				["<c-Right>"] = function(win)
					win:resize("width", 2)
				end,
				-- decrease width
				["<c-Left>"] = function(win)
					win:resize("width", -2)
				end,
				-- increase height
				["<c-Up>"] = function(win)
					win:resize("height", 2)
				end,
				-- decrease height
				["<c-Down>"] = function(win)
					win:resize("height", -2)
				end,
			},
		}

		-- trouble
		for _, pos in ipairs({ "top", "bottom", "left", "right" }) do
			opts[pos] = opts[pos] or {}
			table.insert(opts[pos], {
				ft = "trouble",
				filter = function(_buf, win)
					return vim.w[win].trouble
						and vim.w[win].trouble.position == pos
						and vim.w[win].trouble.type == "split"
						and vim.w[win].trouble.relative == "editor"
						and not vim.w[win].trouble_preview
				end,
			})
		end

		-- snacks terminal
		for _, pos in ipairs({ "top", "bottom", "left", "right" }) do
			opts[pos] = opts[pos] or {}
			table.insert(opts[pos], {
				ft = "snacks_terminal",
				size = { height = 0.4 },
				title = "%{b:snacks_terminal.id}: %{b:term_title}",
				filter = function(_buf, win)
					return vim.w[win].snacks_win
						and vim.w[win].snacks_win.position == pos
						and vim.w[win].snacks_win.relative == "editor"
						and not vim.w[win].trouble_preview
				end,
			})
		end

		return opts
	end,
}
