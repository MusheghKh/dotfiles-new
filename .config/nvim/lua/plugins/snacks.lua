return {
	"folke/snacks.nvim",
	priority = 1000,
	lazy = false,
	opts = {
		-- your configuration comes here
		-- or leave it empty to use the default settings
		-- refer to the configuration section below
		bigfile = { enabled = true },
		dashboard = { enabled = false },
		explorer = { enabled = true, replace_netrw = false },
		indent = { enabled = true },
		input = { enabled = true },
		scratch = {
			ft = "markdown",
		},
		notifier = {
			enabled = true,
			timeout = 3000,
		},
		picker = {
			enabled = true,
			layout = { preset = "ivy" },
			win = {
				input = {
					keys = {
						["<C-s>"] = { "flash", mode = { "n", "i" } },
						["s"] = { "flash" },
					},
				},
			},
			actions = {
				flash = function(picker)
					require("flash").jump({
						pattern = "^",
						label = { after = { 0, 0 } },
						search = {
							mode = "search",
							exclude = {
								function(win)
									return vim.bo[vim.api.nvim_win_get_buf(win)].filetype ~= "snacks_picker_list"
								end,
							},
						},
						action = function(match)
							local idx = picker.list:row2idx(match.pos[1])
							picker.list:_move(idx, true, true)
						end,
					})
				end,
			},
		},
		quickfile = { enabled = true },
		scope = { enabled = true },
		scroll = { enabled = false },
		statuscolumn = { enabled = true },
		words = { enabled = true },
		styles = {
			notification = {
				-- wo = { wrap = true } -- Wrap notifications
			},
		},
	},
	keys = {

		-- Buffers

		{
			"<leader>bs",
			function()
				Snacks.picker.lines()
			end,
			desc = "Search in buffer",
		},
		{
			"<leader>bl",
			function()
				Snacks.picker.buffers({ current = false })
			end,
			desc = "Buffers",
		},
		{
			"<leader>bS",
			function()
				Snacks.picker.grep_buffers()
			end,
			desc = "Marks",
		},
		{
			"<leader>bk",
			function()
				Snacks.bufdelete()
			end,
			desc = "Delete Buffer",
		},
		{
			"<leader>bcc",
			function()
				Snacks.bufdelete.other()
			end,
			desc = "Delete other buffers",
			mode = "n",
		},
		{
			"<leader>bK",
			function()
				Snacks.bufdelete.all()
			end,
			desc = "Delete all buffers",
			mode = "n",
		},

		-- Find and Files

		{
			"<leader>ff",
			function()
				Snacks.picker.files({ hidden = true })
			end,
			desc = "Files",
		},
		{
			"<leader>fF",
			function()
				Snacks.explorer()
			end,
			desc = "Files",
		},

		{
			"<leader>fg",
			function()
				Snacks.picker.git_files()
			end,
			desc = "Git files",
		},
		{
			"<leader>fo",
			function()
				Snacks.picker.recent()
			end,
			desc = "Recent",
		},
		{
			"<leader>fs",
			function()
				Snacks.picker.grep({ hidden = true })
			end,
			desc = "Search in files",
		},
		{
			"<leader>fl",
			function()
				Snacks.picker.resume()
			end,
			desc = "Resume",
		},
		{
			"<leader>fz",
			function()
				Snacks.picker.zoxide()
			end,
			desc = "Zoxide",
		},

		-- Environment
		{
			"<leader>ec",
			function()
				Snacks.picker.command_history()
			end,
			desc = "Command History",
		},
		{
			"<leader>e.",
			function()
				Snacks.scratch()
			end,
			desc = "Toggle Scratch Buffer",
		},
		{
			"<leader>e>",
			function()
				Snacks.scratch({
					ft = "lua",
				})
			end,
			desc = "Select Scratch Buffer",
		},
		{
			"<leader>en",
			function()
				Snacks.notifier.show_history()
			end,
			desc = "Notification History",
		},
		{
			"<leader>eC",
			function()
				Snacks.picker.commands()
			end,
			desc = "Commands",
		},
		{
			"<leader>ec",
			function()
				Snacks.picker.command_history()
			end,
			desc = "Command History",
		},
		{
			"<leader>eh",
			function()
				Snacks.picker.help()
			end,
			desc = "Help",
		},
		{
			"<leader>ej",
			function()
				Snacks.picker.jumps()
			end,
			desc = "Jumps",
		},
		{
			"<leader>em",
			function()
				Snacks.picker.marks()
			end,
			desc = "Marks",
		},
		{
			"<leader>ek",
			function()
				Snacks.picker.keymaps()
			end,
			desc = "Keymaps",
		},
		{
			"<leader>fP",
			function()
				Snacks.picker.projects({
					dev = { "~/Code" },
				})
			end,
			desc = "Projects",
		},
		{
			"<leader>eu",
			function()
				Snacks.picker.undo({
					win = {
						preview = { wo = { number = false, relativenumber = false, signcolumn = "no" } },
						input = {
							keys = {
								["<c-y>"] = { "yank_add", mode = { "n", "i" } },
								["<c-d>"] = { "yank_del", mode = { "n", "i" } },
							},
						},
					},
				})
			end,
			desc = "Undo",
		},

		-- Zen mode

		{
			"<leader>zz",
			function()
				Snacks.zen()
			end,
			desc = "Toggle Zen Mode",
		},
		{
			"<leader>zZ",
			function()
				Snacks.zen.zoom()
			end,
			desc = "Toggle Zoom",
		},

		-- Code

		{
			"<leader>cR",
			function()
				Snacks.rename.rename_file()
			end,
			desc = "Rename File",
		},
		{
			"<leader>cs",
			function()
				Snacks.picker.lsp_symbols()
			end,
			desc = "LSP Symbols",
		},

		-- Git

		{
			"<leader>gb",
			function()
				Snacks.git.blame_line()
			end,
			desc = "Git Blame Line",
		},
		{
			"<leader>gf",
			function()
				Snacks.lazygit.log_file()
			end,
			desc = "Lazygit Current File History",
		},
		{
			"<leader>gg",
			function()
				Snacks.lazygit()
			end,
			desc = "Lazygit",
		},

		-- Terminal

		{
			"<c-/>",
			function()
				Snacks.terminal()
			end,
			desc = "Toggle Terminal",
		},
		{
			"<c-_>",
			function()
				Snacks.terminal()
			end,
			desc = "which_key_ignore",
		},
	},
	init = function()
		vim.api.nvim_create_autocmd("User", {
			pattern = "VeryLazy",
			callback = function()
				-- Setup some globals for debugging (lazy-loaded)
				_G.dd = function(...)
					Snacks.debug.inspect(...)
				end
				_G.bt = function()
					Snacks.debug.backtrace()
				end
				-- vim.print = _G.dd -- Override print to use snacks for `:=` command

				-- Create some toggle mappings
				Snacks.toggle.option("spell", { name = "Spelling" }):map("<leader>es")

				vim.opt.list = true
				vim.opt.listchars:append("space:⋅")
				vim.opt.listchars:append("eol:↴")
			end,
		})
	end,
}
