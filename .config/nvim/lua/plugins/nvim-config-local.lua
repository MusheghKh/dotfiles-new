-- for directory local config

return {
	"klen/nvim-config-local",
	lazy = false,
	-- event = {
	-- 	-- "BufReadPre",
	-- 	-- "BufAdd",
	-- 	-- "BufNew",
	-- 	"VimEnter"
	-- },
	-- cmd = {
	-- 	"ConfigEdit",
	-- 	"ConfigTrust",
	-- 	"ConfigIgnore",
	-- 	"ConfigSource",
	-- },
	config = true,
}
