return {
	"danymat/neogen",
	lazy = true,
	dependencies = "nvim-treesitter/nvim-treesitter",
	keys = {
		{ "<leader>cD", "<cmd>lua require('neogen').generate()<cr>", desc = "Generate doc" },
	},
	opts = {
		snippet_engine = "luasnip",
	},
}
