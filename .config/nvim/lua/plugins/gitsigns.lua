return {
	"lewis6991/gitsigns.nvim",
	-- version = "*",
	lazy = true,
	event = {
		"User SuperLazy",
		"LspAttach",
	},
	opts = {
		on_attach = function(bufnr)
			local gs = package.loaded.gitsigns

			-- Navigation
			vim.keymap.set("n", "]g", function()
				if vim.wo.diff then
					return "]g"
				end
				vim.schedule(function()
					gs.next_hunk()
				end)
				return "<Ignore>"
			end, { expr = true })

			vim.keymap.set("n", "[g", function()
				if vim.wo.diff then
					return "[g"
				end
				vim.schedule(function()
					gs.prev_hunk()
				end)
				return "<Ignore>"
			end, { expr = true })

			-- vim.keymap.set("n", "]g", gs.next_hunk, { expr = true, buffer = bufnr })
			--
			-- vim.keymap.set("n", "[g", gs.prev_hunk, { expr = true, buffer = bufnr })

			-- Actions

			vim.keymap.set("n", "<leader>gs", gs.stage_hunk, { desc = "Stage hunk", buffer = bufnr })
			vim.keymap.set("v", "<leader>gs", function()
				gs.stage_hunk({ vim.fn.line("."), vim.fn.line("v") })
			end, { desc = "Stage hunk", buffer = bufnr })
			vim.keymap.set("n", "<leader>gS", gs.stage_buffer, { desc = "Stage buffer", buffer = bufnr })

			vim.keymap.set("n", "<leader>gr", gs.reset_hunk, { desc = "Reset hunk", buffer = bufnr })
			vim.keymap.set("v", "<leader>gr", function()
				gs.reset_hunk({ vim.fn.line("."), vim.fn.line("v") })
			end, { desc = "Reset hunk", buffer = bufnr })
			vim.keymap.set("n", "<leader>gR", gs.reset_buffer, { desc = "Reset buffer", buffer = bufnr })

			vim.keymap.set("n", "<leader>gu", gs.undo_stage_hunk, { desc = "Undo stage hunk", buffer = bufnr })

			vim.keymap.set("n", "<leader>gp", gs.preview_hunk_inline, { desc = "Preview Hunk", buffer = bufnr })
			vim.keymap.set("n", "<leader>gP", gs.diffthis, { desc = "Show hunks" })

			vim.keymap.set("n", "<leader>gh", "<cmd>Gitsigns setqflist<cr>", { desc = "Trouble hunks" })

			vim.keymap.set("n", "<leader>gB", function()
				gs.blame_line({ full = true })
			end, { desc = "Blame line", buffer = bufnr })

			-- vim.keymap.set(
			-- 	"n",
			-- 	"<leader>gtb",
			-- 	gs.toggle_current_line_blame,
			-- 	{ desc = "Toggle current blame line", buffer = bufnr }
			-- )

			-- vim.keymap.set("n", "<leader>gD", function()
			-- 	gs.diffthis("~")
			-- end, { desc = "Diff This", buffer = bufnr })

			-- vim.keymap.set("n", "<leader>gtd", gs.toggle_deleted, { desc = "Toggle deleted", buffer = bufnr })

			-- Text object
			-- vim.keymap.set({ "o", "x" }, "ih", ":<C-U>Gitsigns select_hunk<cr>")
		end,
	},
}
