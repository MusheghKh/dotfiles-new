return {
	"linrongbin16/gitlinker.nvim",
	cmd = "GitLink",
	enabled = true,
	keys = {
		{
			"<leader>gy",
			"<cmd>GitLink remote=origin<cr>",
			desc = "Copy git link",
			mode = { "n", "x" },
		},
		{
			"<leader>gY",
			"<cmd>GitLink current_branch remote=origin<cr>",
			desc = "Copy git link",
			mode = { "n", "x" },
		},
	},
	config = function()
		local ok, result = pcall(require, "private.gitlinker_new")

		if ok then
			require("gitlinker").setup(result)
			return
		else
			vim.notify("gitlinker: additional routes are not set up", vim.log.levels.ERROR)
		end

		require("gitlinker").setup()
	end,
}
