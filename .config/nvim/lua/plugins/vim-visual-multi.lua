return {
	"mg979/vim-visual-multi",
	branch = "master",
	lazy = true,
	event = {
		"InsertEnter",
	},
	keys = {
		{ "<C-n>" },
		{ "<C-Down>" },
		{ "<C-Up>" },
	},
	config = function()
		local VM_maps = {
			["Find Under"] = "<C-n>",
			["Find Subword Under"] = "<C-n>",
			["Undo"] = "u",
			["Redo"] = "<C-r>",
		}

		-- vim.g.VM_leader = "<leader>m"
		vim.g.VM_mouse_mappings = 0
		vim.g.VM_theme = "codedark"
		vim.g.VM_maps = VM_maps
	end,
}
