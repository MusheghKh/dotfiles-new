return {
	"nvimtools/hydra.nvim",
	lazy = true,
	keys = {
		{ "<leader>w" },
	},
	config = function()
		local hydra = require("hydra")
		hydra.setup({})
		hydra({
			config = {
				hint = {
					type = "window",
					position = "middle",
				},
			},
			name = "Window",
			mode = "n",
			body = "<leader>w",
			heads = {
				{ "K", "<C-w>+<C-w>+", { desc = "Increase height" } },
				{ "J", "<C-w>-<C-w>-", { desc = "Decrease height" } },
				{ "L", "<C-w>><C-w>>", { desc = "Increase width" } },
				{ "H", "<C-w><<C-w><", { desc = "Decrease width" } },
				{ "<Esc>", nil, { exit = true } },
				{ "<CR>", nil, { exit = true } },
			},
		})
	end,
}
