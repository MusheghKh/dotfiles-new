return {
	"habamax/vim-asciidoctor",
	lazy = true,
	ft = {
		"asciidoctor",
		"adoc",
	},
	cmd = {
		"AsciidoctorOpenRAW",
		"AsciidoctorOpenPDF",
		"AsciidoctorOpenHTML",
		"AsciidoctorOpenDOCX",
		"Asciidoctor2HTML",
		"Asciidoctor2PDF",
		"Asciidoctor2DOCX",
	},
	config = function()
		vim.api.nvim_create_augroup("User_asciidoctor", { clear = true })

		vim.api.nvim_create_autocmd("BufEnter", {
			group = "User_asciidoctor",
			pattern = { "*.adoc", "*.asciidoc" },
			callback = function(ev)
				vim.print(ev)
				vim.print(ev.buffer)
				vim.keymap.set("n", "<leader>aR", "<cmd>AsciidoctorOpenRAW<CR>", { buffer = ev.buf, desc = "Open Raw" })
				vim.keymap.set("n", "<leader>aP", "<cmd>AsciidoctorOpenPDF<CR>", { buffer = ev.buf, desc = "Open PDF" })
				vim.keymap.set("n", "<leader>aH", "<cmd>AsciidoctorOpenHTML<CR>", { buffer = ev.buf, desc = "Open HTML" })
				vim.keymap.set("n", "<leader>aX", "<cmd>AsciidoctorOpenDOCX<CR>", { buffer = ev.buf, desc = "Open DOCX" })
				vim.keymap.set("n", "<leader>ah", "<cmd>Asciidoctor2HTML<CR>", { buffer = ev.buf, desc = "To HTML" })
				vim.keymap.set("n", "<leader>ap", "<cmd>Asciidoctor2PDF<CR>", { buffer = ev.buf, desc = "To PDF" })
				vim.keymap.set("n", "<leader>ax", "<cmd>Asciidoctor2DOCX<CR>", { buffer = ev.buf, desc = "To DOCX" })
			end,
		})
	end,
}
