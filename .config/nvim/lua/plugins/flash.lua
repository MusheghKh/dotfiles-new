return {
	"folke/flash.nvim",
	lazy = true,
	opts = {
		search = {
			-- search/jump in all windows
			multi_window = false,
		},
		prompt = {
			enabled = true,
		},
		highlight = {
			backdrop = true,
		},
		modes = {
			search = {
				enabled = false,
				highlight = { backdrop = true },
			},
			char = {
				enabled = false,
				-- autohide = true,
				highlight = {
					backdrop = false,
				},
			},
		},
	},
	keys = {
		{
			"s",
			mode = { "n", "x", "o" },
			function()
				require("flash").jump()
			end,
			desc = "Flash",
		},
		{
			"S",
			mode = { "n", "x", "o" },
			function()
				require("flash").treesitter()
			end,
			desc = "Flash Treesitter",
		},
		{
			"r",
			mode = "o",
			function()
				require("flash").remote()
			end,
			desc = "Remote Flash",
		},
		{
			"R",
			mode = { "o", "x" },
			function()
				require("flash").treesitter_search()
			end,
			desc = "Treesitter Search",
		},
		{
			"<c-s>",
			mode = { "c" },
			function()
				require("flash").toggle()
			end,
			desc = "Toggle Flash Search",
		},
		{ "f" },
		{ "F" },
		{ "t" },
		{ "T" },
		{ ";" },
		{ "," },
		{ "/" },
		{ "?" },
		{
			"<C-x><C-s>",
			function()
				local keys = vim.api.nvim_replace_termcodes("<C-\\><C-n>", true, false, true)
				vim.api.nvim_feedkeys(keys, "nt", false)
				vim.defer_fn(function()
					require("flash").jump()
				end, 1)
			end,
			mode = { "t", "n" },
			desc = "Flash from Term",
		},
	},
}
