return {
	"mfussenegger/nvim-lint",
	dependencies = {
		"williamboman/mason.nvim",
	},
	lazy = true,
	event = {
		"LspAttach",
		"CursorMoved",
		-- "FileReadPre",
		-- "BufWritePre",
		-- "BufRead",
		-- "BufNew",
		-- "BufAdd",
		-- "BufReadPost",
		-- "VeryLazy",
	},
	config = function()
		-- -- Setup pylint path
		-- local pylint_paths = {}
		--
		-- -- pylint venv path
		-- local venv_path = vim.fn.getenv("VIRTUAL_ENV")
		-- if venv_path ~= vim.NIL then
		-- 	table.insert(pylint_paths, venv_path .. "/bin/pylint")
		-- end
		--
		-- -- Add conda path
		-- local conda_prefix = vim.fn.getenv("CONDA_PREFIX")
		-- if conda_prefix ~= vim.NIL then
		-- 	table.insert(pylint_paths, conda_prefix .. "/bin/pylint")
		-- end
		--
		-- for _, path in ipairs(pylint_paths) do
		-- 	if vim.fn.executable(path) == 1 then
		-- 		local pylint = require("lint").linters.pylint
		-- 		pylint.cmd = path
		-- 	end
		-- end

		vim.api.nvim_create_user_command("LinterInfo", function()
			local runningLinters = table.concat(require("lint").get_running(), "\n")
			vim.notify(runningLinters, vim.log.levels.INFO, { title = "nvim-lint" })
		end, {})

		local linters = {
			lua = {},
			cmake = { "cmakelint" },
			proto = { "buf_lint" },
			-- ruby = { "rubocop" },
			ruby = {},
			c = { "cppcheck", "cpplint" },
			cpp = { "cppcheck", "cpplint" },
			python = { "pylint" },
			go = { "golangcilint" },
			rust = {},
			-- javascript = { "eslint_d" },
			-- typescript = { "eslint_d" },
			javascript = {},
			typescript = {},
			groovy = { "npm-groovy-lint" },
			java = { "checkstyle" },
			perl = {},
			haskell = {},
			nix = {},
			vim = { "vint" },

			sql = { "sqlfluff" },
			sh = {},
			bash = {},
			zsh = { "zsh" },
			awk = { "gawk" },
			ps1 = {},

			css = { "stylelint" },
			scss = { "stylelint" },
			sass = { "stylelint" },
			less = { "stylelint" },

			["yaml.ansible"] = { "ansible_lint" },
			dockerfile = { "hadolint" },
			yaml = { "yamllint" },
			json = {},
			puppet = { "puppet-lint" },
			make = { "checkmake" },
			terraform = { "tflint", "tfsec" },

			systemd = { "systemdlint" },

			editorconfig = { "editorconfig-checker" },

			adoc = {},
			asciidoctor = {},
			tex = {},
			-- markdown = { "markdownlint" },
			markdown = {},
			gitcommit = {},
		}

		for _, value in pairs(linters) do
			table.insert(value, "codespell")
			table.insert(value, "typos")
		end

		local non_lint = {
			toggleterm = {},
		}

		for _, value in pairs(non_lint) do
			table.insert(linters, value)
		end

		require("lint").linters_by_ft = linters

		vim.schedule_wrap(function()
			require("lint").try_lint()
		end)

		vim.api.nvim_create_augroup("nvim-lint-group", { clear = true })
		vim.api.nvim_create_autocmd({ "InsertLeave", "BufEnter", "BufWritePost" }, {
			group = "nvim-lint-group",
			callback = function()
				require("lint").try_lint()
			end,
		})
	end,
}
