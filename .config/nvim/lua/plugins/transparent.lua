return {
	"xiyaowong/transparent.nvim",
	lazy = true,
	keys = {
		{ "<leader>et", "<cmd>TransparentToggle<cr>", desc = "TransparentToggle" },
	},
}
