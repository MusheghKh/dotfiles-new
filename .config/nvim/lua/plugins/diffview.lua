return {
	"sindrets/diffview.nvim",
	dependencies = {
		"nvim-lua/plenary.nvim",
	},
	lazy = true,
	cmd = {
		"DiffviewOpen",
		"DiffviewClose",
		"DiffviewLog",
		"DiffviewRefresh",
		"DiffviewFocusFiles",
		"DiffviewFileHistory",
		"DiffviewToggleFiles",
	},
	keys = {
		{ "<leader>gd", ":DiffviewOpen ", desc = "show diff" },
		{ "<leader>gD", ":DiffviewFileHistory ", desc = "show diff" },
	},
	opts = {
		view = {
			-- Configure the layout and behavior of different types of views.
			-- Available layouts:
			--  'diff1_plain'
			--    |'diff2_horizontal'
			--    |'diff2_vertical'
			--    |'diff3_horizontal'
			--    |'diff3_vertical'
			--    |'diff3_mixed'
			--    |'diff4_mixed'
			-- For more info, see ':h diffview-config-view.x.layout'.
			merge_tool = {
				-- Config for conflicted files in diff views during a merge or rebase.
				layout = "diff3_mixed",
				disable_diagnostics = true, -- Temporarily disable diagnostics for conflict buffers while in the view.
			},
		},
		keymaps = {
			disable_defaults = false, -- Disable the default keymaps
			view = {
				{
					"n",
					"q",
					function()
						vim.cmd("DiffviewClose")
					end,
					{ desc = "Close Diffview" },
				},
				{
					"n",
					"<leader>e",
					false,
					{ desc = "Disable jump to file" },
				},
			},
			file_panel = {
				{
					"n",
					"q",
					function()
						vim.cmd("DiffviewClose")
					end,
					{ desc = "Close Diffview" },
				},
				{
					"n",
					"<leader>e",
					false,
					{ desc = "Disable jump to file" },
				},
			},
			file_history_panel = {
				{
					"n",
					"q",
					function()
						vim.cmd("DiffviewClose")
					end,
					{ desc = "Close Diffview" },
				},
				{
					"n",
					"<leader>e",
					false,
					{ desc = "Disable jump to file" },
				},
			},
		},
	},
}
