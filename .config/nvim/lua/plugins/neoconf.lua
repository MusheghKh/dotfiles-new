return {
	"folke/neoconf.nvim",
	lazy = true,
	dependencies = {
		"folke/lazydev.nvim",
	},
	cmd = {
		"Neoconf",
	},
	config = true,
}
