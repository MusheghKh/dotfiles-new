return {
	"folke/which-key.nvim",
	lazy = true,
	event = {
		"User SuperLazy",
	},
	-- config = true,
	config = function()
		local wk = require("which-key")
		wk.setup()
	end,
}
