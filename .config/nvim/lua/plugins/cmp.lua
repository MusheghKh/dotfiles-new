return {
	"hrsh7th/nvim-cmp",
	dependencies = {
		-- From nvim cmp docs
		-- "neovim/nvim-lspconfig",
		"hrsh7th/cmp-nvim-lsp",
		"hrsh7th/cmp-buffer",
		"hrsh7th/cmp-path",
		"hrsh7th/cmp-cmdline",
		"L3MON4D3/LuaSnip",
		"saadparwaiz1/cmp_luasnip",

		-- "hrsh7th/cmp-nvim-lsp-signature-help",
		"rcarriga/cmp-dap",

		-- Symbols
		"onsails/lspkind.nvim",
	},
	lazy = true,
	event = {
		"InsertEnter",
		"CmdlineEnter",
	},
	init = function()
		vim.cmd("set completeopt=menu,menuone,noselect")
	end,
	config = function()
		local cmp = require("cmp")
		local luasnip = require("luasnip")
		local lspkind = require("lspkind")

		local has_words_before = function()
			local line, col = unpack(vim.api.nvim_win_get_cursor(0))
			return col ~= 0 and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match("%s") == nil
		end

		cmp.setup({
			enabled = function()
				return vim.api.nvim_get_option_value("buftype", { buf = 0 }) ~= "prompt" or require("cmp_dap").is_dap_buffer()
			end,
			snippet = {
				-- REQUIRED - you must specify a snippet engine
				expand = function(args)
					-- vim.fn["vsnip#anonymous"](args.body) -- For `vsnip` users.
					luasnip.lsp_expand(args.body) -- For `luasnip` users.
					-- require('snippy').expand_snippet(args.body) -- For `snippy` users.
					-- vim.fn["UltiSnips#Anon"](args.body) -- For `ultisnips` users.
				end,
			},
			window = {
				completion = cmp.config.window.bordered(),
				documentation = cmp.config.window.bordered(),
			},
			formatting = {
				format = lspkind.cmp_format({
					mode = "symbol_text", -- show only symbol annotations
					maxwidth = 100, -- prevent the popup from showing more than provided characters (e.g 50 will not show more than 50 characters)
					ellipsis_char = "...", -- when popup menu exceed maxwidth, the truncated part would show ellipsis_char instead (must define maxwidth first)
					symbol_map = { Codeium = "" },

					-- The function below will be called before any actual modifications from lspkind
					-- so that you can provide more controls on popup customization. (See [#30](https://github.com/onsails/lspkind-nvim/pull/30))
					-- before = function(entry, vim_item)
					-- 	return vim_item
					-- end,
				}),
			},
			-- mapping = cmp.mapping.preset.insert({
			mapping = {
				["<C-b>"] = cmp.mapping.scroll_docs(-4),
				["<C-f>"] = cmp.mapping.scroll_docs(4),
				["<C-e>"] = cmp.mapping.abort(),
				-- ["<Esc>"] = cmp.map
				["<CR>"] = cmp.mapping.confirm({ select = false }), -- Accept currently selected item. Set `select` to `false` to only confirm explicitly selected items.
				-- ["<C-Space>"] = cmp.mapping(
				-- 	-- cmp.mapping.select_next_item({ behavior = cmp.SelectBehavior.Select }),
				-- 	function(fallback)
				-- 		if luasnip.locally_jumpable(1) then
				-- 			luasnip.jump(1)
				-- 		else
				-- 			fallback()
				-- 		end
				-- 	end,
				-- 	{ "i", "s", "c" }
				-- ),
				["<Tab>"] = cmp.mapping(
					-- cmp.mapping.select_next_item({ behavior = cmp.SelectBehavior.Select }),
					function(fallback)
						if cmp.visible() then
							cmp.select_next_item({ behavior = cmp.SelectBehavior.Insert })
						-- elseif luasnip.locally_jumpable(1) then
						-- 	luasnip.jump(1)
						else
							fallback()
						end
					end,
					{ "i", "s", "c" }
				),
				["<S-Tab>"] = cmp.mapping(
					-- cmp.mapping.select_next_item({ behavior = cmp.SelectBehavior.Select }),
					function(fallback)
						if cmp.visible() then
							cmp.select_prev_item({ behavior = cmp.SelectBehavior.Insert })
						-- elseif luasnip.locally_jumpable(-1) then
						-- 	luasnip.jump(-1)
						else
							fallback()
						end
					end,
					{ "i", "s", "c" }
				),
				["<C-j>"] = cmp.mapping(function(fallback)
					if cmp.visible() then
						cmp.select_next_item({ behavior = cmp.SelectBehavior.Insert })
					elseif luasnip.locally_jumpable(1) then
						luasnip.jump(1)
					else
						fallback()
					end
				end, { "i", "s", "c" }),
				["<C-k>"] = cmp.mapping(function(fallback)
					if cmp.visible() then
						cmp.select_prev_item({ behavior = cmp.SelectBehavior.Insert })
					elseif luasnip.locally_jumpable(-1) then
						luasnip.jump(-1)
					else
						fallback()
					end
				end, { "i", "s", "c" }),
				["<C-Space>"] = cmp.mapping(function(fallback)
					if luasnip.locally_jumpable(1) then
						luasnip.jump(1)
					elseif has_words_before() then
						cmp.complete()
					else
						fallback()
					end
				end, { "i", "s" }),
				-- -- ["<C-p>"] = cmp.mapping(function(fallback)
				-- 	if luasnip.locally_jumpable(-1) then
				-- 		luasnip.jump(-1)
				-- 	else
				-- 		fallback()
				-- 	end
				-- end, { "i", "s" }),
				-- }),
			},
			sources = cmp.config.sources({
				{ name = "nvim_lsp" },
				{ name = "luasnip" }, -- For luasnip users.
				{ name = "render-markdown"},
				-- { name = "codeium" },
				-- { name = "nvim_lsp_signature_help" },
			}, {
				{ name = "buffer" },
			}),
		})

		-- Use buffer source for `/` and `?` (if you enabled `native_menu`, this won't work anymore).
		cmp.setup.cmdline({ "/", "?" }, {
			-- mapping = cmp.mapping.preset.cmdline(),
			sources = {
				{
					name = "buffer",
				},
			},
		})

		-- Use cmdline & path source for ':' (if you enabled `native_menu`, this won't work anymore).
		cmp.setup.cmdline(":", {
			-- mapping = cmp.mapping.preset.cmdline(),
			sources = cmp.config.sources({
				{ name = "path" },
			}, {
				{ name = "cmdline" },
			}),
		})

		cmp.setup.filetype({ "dap-repl", "dapui_watches", "dapui_hover" }, {
			sources = {
				{ name = "dap" },
			},
		})
	end,
}
