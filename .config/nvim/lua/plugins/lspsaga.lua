return {
	"nvimdev/lspsaga.nvim",
	dependencies = {
		"nvim-treesitter/nvim-treesitter",
		"nvim-tree/nvim-web-devicons",
	},
	lazy = true,
	event = {
		"LspAttach",
	},
	keys = {
		-- { "gf", "<cmd>Lspsaga finder<cr>", desc = "Finder" },
		{ "]e", "<cmd>Lspsaga diagnostic_jump_next<cr>", desc = "Lspsaga Diag next" },
		{ "[e", "<cmd>Lspsaga diagnostic_jump_prev<cr>", desc = "Lspsaga Diag prev" },
		{ "gd", "<cmd>Lspsaga goto_definition<cr>", desc = "Definition" },
		{ "gJ", "<cmd>Lspsaga finder<cr>", desc = "Lspsaga Finder" },
		{ "<leader>cT", "<cmd>Lspsaga goto_type_definition<cr>", desc = "Lspsaga Type Definition" },
		{ "K", "<cmd>Lspsaga hover_doc<cr>", desc = "Lspsaga Hover" },
		{ "<leader>cr", "<cmd>Lspsaga rename mode=n<cr>", desc = "Lspsaga Rename" },
		{ "gi", "<cmd>Lspsaga incoming_calls<cr>", desc = "Lspsaga Incoming calls" },
		{ "go", "<cmd>Lspsaga outgoing_calls<cr>", desc = "Lspsaga Incoming calls" },
		{ "gx", "<cmd>Lspsaga code_action<cr>", desc = "Lspsaga Code action" },
		{ "<leader>co", "<cmd>Lspsaga outline<cr>", desc = "Lspsaga Outline" },
		{ "gl", "<cmd>Lspsaga show_line_diagnostics<cr>", desc = "Lspsaga Line diagnostics" },
	},
	opts = {
		symbol_in_winbar = {
			enable = true,
		},
		rename = {
			in_select = false,
		},
		code_action = {
			show_server_name = true,
			extend_gitsigns = false,
		},
		lightbulb = {
			sign = false,
			virtual_text = true,
		},
	},
	config = function(_, opts)
		require("lspsaga").setup(opts)

		vim.diagnostic.config({
			severity_sort = true,
		})
	end,
}
