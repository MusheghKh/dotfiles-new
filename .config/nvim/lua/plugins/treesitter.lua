return {
	"nvim-treesitter/nvim-treesitter",
	build = ":TSUpdate",
	lazy = true,
	event = {
		"FileType"
	},
	config = function()
		-- local ts_repeat_move = require("nvim-treesitter.textobjects.repeatable_move")
		local treesitter_configs = require("nvim-treesitter.configs")

		local swap_next, swap_prev = (function()
			local swap_objects = {
				p = "@parameter.inner",
				f = "@function.outer",
				b = "@block.outer",
			}

			local n, p = {}, {}
			for key, obj in pairs(swap_objects) do
				n[string.format("gn%s", key)] = obj
				p[string.format("gp%s", key)] = obj
			end

			return n, p
		end)()

		treesitter_configs.setup({
			-- A list of parser names, or "all"
			-- ensure_installed = "all",

			-- Install parsers synchronously (only applied to `ensure_installed`)
			sync_install = false,

			-- Automatically install missing parsers when entering buffer
			-- Recommendation: set to false if you don't have `tree-sitter` CLI installed locally
			auto_install = true,

			-- List of parsers to ignore installing (for "all"),
			ignore_install = { "tmux" },

			---- If you need to change the installation directory of the parsers (see -> Advanced Setup)
			-- parser_install_dir = "/some/path/to/store/parsers", -- Remember to run vim.opt.runtimepath:append("/some/path/to/store/parsers")!

			playground = {
				enable = true,
				disable = {},
				updatetime = 25, -- Debounced time for highlighting nodes in the playground from source code
				persist_queries = false, -- Whether the query persists across vim sessions
				keybindings = {
					toggle_query_editor = "o",
					toggle_hl_groups = "i",
					toggle_injected_languages = "t",
					toggle_anonymous_nodes = "a",
					toggle_language_display = "I",
					focus_language = "f",
					unfocus_language = "F",
					update = "R",
					goto_node = "<cr>",
					show_help = "?",
				},
			},
			incremental_selection = {
				enable = true,
				keymaps = {
					init_selection = "<cr>",
					node_incremental = "<cr>",
					-- scope_incremental = "<c-s>",
					node_decremental = "<bs>",
				},
			},

			highlight = {
				-- `false` will disable the whole extension
				enable = true,

				-- NOTE: these are the names of the parsers and not the filetype. (for example if you want to
				-- disable highlighting for the `tex` filetype, you need to include `latex` in this list as this is
				-- the name of the parser)
				-- list of language that will be disabled
				-- disable = { "perl" },

				-- Or use a function for more flexibility, e.g. to disable slow treesitter highlight for large files
				disable = function(lang, buf)
					-- if lang == "perl" then
					-- 	return true
					-- end

          return false

					-- local max_filesize = 100 * 1024 -- 100 KB
					-- local ok, stats = pcall(vim.loop.fs_stat, vim.api.nvim_buf_get_name(buf))
					-- if ok and stats and stats.size > max_filesize then
					-- 	return true
					-- end
				end,

				-- Setting this to true will run `:h syntax` and tree-sitter at the same time.
				-- Set this to `true` if you depend on 'syntax' being enabled (like for indentation).
				-- Using this option may slow down your editor, and you may see some duplicate highlights.
				-- Instead of true it can also be a list of languages
				-- additional_vim_regex_highlighting = false,

				-- Required for spellcheck, some LaTex highlights and
				-- code block highlights that do not have ts grammar
				additional_vim_regex_highlighting = {},
			},
			-- textobjects = {
			-- 	move = {
			-- 		enable = true,
			-- 		set_jumps = true, -- whether to set jumps in the jumplist
			-- 		goto_next = {
			-- 			["]]"] = "@class.outer",
			-- 			["]p"] = "@parameter.outer",
			-- 			["]f"] = "@function.outer",
			-- 			["]b"] = "@block.outer",
			-- 			["]i"] = "@conditional.outer",
			-- 			["]l"] = "@loop.outer",
			-- 			["]c"] = "@class.outer",
			-- 			["]a"] = "@assignment.outer",
			-- 		},
			-- 		goto_previous = {
			-- 			["[["] = "@class.outer",
			-- 			["[p"] = "@parameter.outer",
			-- 			["[f"] = "@function.outer",
			-- 			["[b"] = "@block.outer",
			-- 			["[i"] = "@conditional.outer",
			-- 			["[l"] = "@loop.outer",
			-- 			["[c"] = "@class.outer",
			-- 			["[a"] = "@assignment.outer",
			-- 		},
			-- 	},
			-- 	select = {
			-- 		enable = true,
			--
			-- 		-- automatically jump forward to textobj, similar to targets.vim
			-- 		lookahead = true,
			--
			-- 		keymaps = {
			-- 			-- you can use the capture groups defined in textobjects.scm
			-- 			["af"] = { query = "@function.outer", desc = "select outer part of function" },
			-- 			["if"] = { query = "@function.inner", desc = "select inner part of function" },
			--
			-- 			["ib"] = { query = "@block.inner", desc = "Select inner block" },
			-- 			["ab"] = { query = "@block.outer", desc = "Select inner block" },
			--
			-- 			["ii"] = { query = "@conditional.inner", desc = "Select inner if" },
			-- 			["ai"] = { query = "@conditional.outer", desc = "Select inner if" },
			--
			-- 			["il"] = { query = "@loop.inner", desc = "Select inner loop" },
			-- 			["al"] = { query = "@loop.outer", desc = "Select inner loop" },
			--
			-- 			["aa"] = { query = "@assignment.outer", desc = "select outer part of assignment" },
			-- 			["ia"] = { query = "@assignment.inner", desc = "select inner part of assignment" },
			--
			-- 			["ac"] = { query = "@class.outer", desc = "select outer part of class" },
			-- 			["ic"] = { query = "@class.inner", desc = "select inner part of class" },
			--
			-- 			["iq"] = { query = "@scope", query_group = "locals", desc = "Select language scope" },
			-- 		},
			--
			-- 		-- you can choose the select mode (default is charwise 'v')
			-- 		--
			-- 		-- can also be a function which gets passed a table with the keys
			-- 		-- * query_string: eg '@function.inner'
			-- 		-- * method: eg 'v' or 'o'
			-- 		-- and should return the mode ('v', 'v', or '<c-v>') or a table
			-- 		-- mapping query_strings to modes.
			-- 		selection_modes = {
			-- 			["@parameter.outer"] = "v", -- charwise
			-- 			["@function.outer"] = "V", -- linewise
			-- 			["@class.outer"] = "<c-v>", -- blockwise
			-- 		},
			--
			-- 		-- if you set this to `true` (default is `false`) then any textobject is
			-- 		-- extended to include preceding or succeeding whitespace. succeeding
			-- 		-- whitespace has priority in order to act similarly to eg the built-in
			-- 		-- `ap`.
			-- 		--
			-- 		-- can also be a function which gets passed a table with the keys
			-- 		-- * query_string: eg '@function.inner'
			-- 		-- * selection_mode: eg 'v'
			-- 		-- and should return true of false
			-- 		include_surrounding_whitespace = false,
			-- 	},
			-- 	swap = {
			-- 		enable = true,
			-- 		swap_next = swap_next,
			-- 		swap_previous = swap_prev,
			-- 	},
			-- 	-- lsp
			-- 	lsp_interop = {
			-- 		enable = true,
			-- 		border = "none",
			-- 		peek_definition_code = {
			-- 			["<leader>cp"] = "@function.outer",
			-- 			["<leader>cP"] = "@class.outer",
			-- 		},
			-- 	},
			-- },
		})

		-- vim way: ; goes to the direction you were moving.
		-- vim.keymap.set({ "n", "x", "o" }, "]]", ts_repeat_move.repeat_last_move)
		-- vim.keymap.set({ "n", "x", "o" }, "[[", ts_repeat_move.repeat_last_move_opposite)

		-- vim.keymap.set({ "n", "x" }, ",", ts_repeat_move.repeat_last_move_previous)

		-- vim.keymap.set({ "n", "x" }, ";", ts_repeat_move.repeat_last_move)
		-- vim.keymap.set({ "n", "x" }, ",", ts_repeat_move.repeat_last_move_opposite)

		-- Optionally, make builtin f, F, t, T also repeatable with ; and ,
		-- vim.keymap.set({ "n", "x" }, "f", ts_repeat_move.builtin_f)
		-- vim.keymap.set({ "n", "x" }, "F", ts_repeat_move.builtin_F)
		-- vim.keymap.set({ "n", "x" }, "t", ts_repeat_move.builtin_t)
		-- vim.keymap.set({ "n", "x" }, "T", ts_repeat_move.builtin_T)

		-- LSP Diagnostic
		-- local next_diag_repeat, prev_diag_repeat =
		-- 	ts_repeat_move.make_repeatable_move_pair(vim.diagnostic.goto_next, vim.diagnostic.goto_prev)
		-- vim.keymap.set({ "n", "x" }, "]e", next_diag_repeat)
		-- vim.keymap.set({ "n", "x" }, "[e", prev_diag_repeat)

		-- Gitsigns
		-- local gs = package.loaded.gitsigns
		-- local next_hunk_repeat, prev_hunk_repeat = ts_repeat_move.make_repeatable_move_pair(gs.next_hunk, gs.prev_hunk)
		-- local next_hunk_repeat, prev_hunk_repeat = ts_repeat_move.make_repeatable_move_pair(function ()
		-- 	vim.schedule(gs.next_hunk)
		-- end, function ()
		-- 	vim.schedule(gs.prev_hunk)
		-- end)

		-- vim.keymap.set({ "n", "x" }, "]g", next_hunk_repeat)
		-- vim.keymap.set({ "n", "x" }, "[g", prev_hunk_repeat)
	end,
}
