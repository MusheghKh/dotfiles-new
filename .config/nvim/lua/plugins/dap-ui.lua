local user_func = require("user.func")

-- local function open_dap_ui()
-- 	require("dapui").open({ reset = true })
-- end
--
-- local function toggle_dap_ui()
-- 	require("dapui").toggle({ reset = true })
-- end
--
-- local function close_dap_ui()
-- 	require("dapui").close()
-- end

return {
	"rcarriga/nvim-dap-ui",
	dependencies = {
		"nvim-neotest/nvim-nio",
		"mfussenegger/nvim-dap",
		"jay-babu/mason-nvim-dap.nvim",
	},
	lazy = true,
	event = {
		"LspAttach",
	},
	keys = {
		{
			"<leader>du",
			function()
				require("dapui").toggle({ reset = true })
			end,
			desc = "[DAP] Toggle UI",
		},
		-- {
		-- 	"<leader>dU",
		-- 	close_dap_ui,
		-- 	desc = "[DAP] Close UI",
		-- },
		{ "<leader>df", "<cmd>lua require'dapui'.float_element()<cr>", desc = "[DAP] Floating UI element" },
		{
			"<leader>dr",
			"<cmd>lua require'dapui'.float_element('repl', { position = 'center', enter = true })<cr>",
			desc = "[DAP] Float REPL",
		},
		{
			"<leader>dc",
			"<cmd>lua require'dapui'.float_element('console', { position = 'center', enter = true })<cr>",
			desc = "[DAP] Float Console",
		},
		{
			"<leader>dx",
			function()
				vim.ui.input({ prompt = "Eval: " }, function(input)
					if input ~= nil and input ~= "" then
						require("dapui").eval(input, { enter = true })
					end
				end)
			end,
			desc = "[DAP] Eval expression",
		},
	},
	config = function()
		local dap, dapui = require("dap"), require("dapui")

		dapui.setup()

		dap.listeners.before.attach.dapui_config = function()
			require("dapui").open({ reset = true })
		end
		dap.listeners.before.launch.dapui_config = function()
			require("dapui").open({ reset = true })
		end
		dap.listeners.before.event_terminated.dapui_config = function()
			require("dapui").close()
		end
		dap.listeners.before.event_exited.dapui_config = function()
			require("dapui").close()
		end
	end,
}
