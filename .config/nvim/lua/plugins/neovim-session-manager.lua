return {
	"Shatur/neovim-session-manager",
	lazy = false,
	keys = {
		{ "<leader>fp", "<cmd>SessionManager load_session<cr>", desc = "Load session" },
	},
	config = function()
		local session_manager = require("session_manager")
		local config = require("session_manager.config")

		session_manager.setup({
			autoload_mode = vim.env.KITTY_SCROLLBACK_NVIM == "true" and config.AutoloadMode.Disabled or config.AutoloadMode.CurrentDir, -- Define what to do when Neovim is started without arguments. See "Autoload mode" section below.
			autosave_ignore_not_normal = true, -- Plugin will not save a session when no buffers are opened, or all of them aren't writable or listed.
			autosave_ignore_dirs = { os.getenv("HOME") }, -- A list of directories where the session will not be autosaved.
			autosave_ignore_filetypes = { -- All buffers of these file types will be closed before the session is saved.
				"gitcommit",
				"gitrebase",
			},
			autosave_ignore_buftypes = {}, -- All buffers of these bufer types will be closed before the session is saved.
			autosave_only_in_session = false, -- Always autosaves session. If true, only autosaves after a session is active.
			load_include_current = false, -- The currently loaded session appears in the load_session UI.
		})

		local au_group = vim.api.nvim_create_augroup("session_manager_group", { clear = true })

		vim.api.nvim_create_autocmd({ "User" }, {
			pattern = "SessionLoadPost",
			group = au_group,
			callback = function()
				vim.defer_fn(function()
					vim.cmd("Neotree show")
					vim.cmd("ConfigLocalSource")
				end, 1)
			end,
		})

		vim.api.nvim_create_autocmd({ "BufWritePre" }, {
			group = au_group,
			callback = function()
				for _, buf in ipairs(vim.api.nvim_list_bufs()) do
					-- Don't save while there's any 'nofile' buffer open.
					if vim.api.nvim_get_option_value("buftype", { buf = buf }) == "nofile" then
						return
					end
				end
				session_manager.save_current_session()
			end,
		})
	end,
}
