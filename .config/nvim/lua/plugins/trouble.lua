return {
	"folke/trouble.nvim",
	dependencies = {
		"nvim-tree/nvim-web-devicons",
	},
	lazy = true,
	event = {
		"LspAttach",
	},
	keys = {
		{ "<leader>cc", "<cmd>Trouble <cr>", desc = "Trouble Toggle" },
		{ "<leader>cd", "<cmd>Trouble diagnostics<cr>", desc = "Trouble document_diagnostics" },
		{ "<leader>cq", "<cmd>Trouble quickfix<cr>", desc = "Trouble quickfix" },
		{ "<leader>cQ", "<cmd>Trouble loclist<cr>", desc = "Trouble loclist" },
		{ "ga", "<cmd>Trouble lsp<cr>", desc = "Trouble LSP" },
		{ "gj", "<cmd>Trouble lsp_references<cr>", desc = "Trouble lsp_references" },
		{ "gk", "<cmd>Trouble lsp_definitions<cr>", desc = "Trouble lsp_definitions" },
		{ "<leader>ct", "<cmd>Trouble lsp_type_definitions<cr>", desc = "Trouble lsp_type_definitions" },
		{ "<leader>ci", "<cmd>Trouble lsp_implementations<cr>", desc = "Trouble lsp_implementations" },
	},
	opts = {
		focus = true,
	},
	specs = {
		"folke/snacks.nvim",
		opts = function(_, opts)
			return vim.tbl_deep_extend("force", opts or {}, {
				picker = {
					actions = require("trouble.sources.snacks").actions,
					win = {
						input = {
							keys = {
								["<c-q>"] = {
									"trouble_open",
									mode = { "n", "i" },
								},
							},
						},
					},
				},
			})
		end,
	},
}
