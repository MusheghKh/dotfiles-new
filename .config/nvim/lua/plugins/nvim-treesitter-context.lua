return {
	"nvim-treesitter/nvim-treesitter-context",
	lazy = true,
	keys = {
		{
			"<leader>ck",
			function()
				require("treesitter-context").go_to_context(vim.v.count1)
			end,
			desc = "Go to context",
		},
	},
	event = "LspAttach",
	opts = {
    max_lines = 2,
  },
}
