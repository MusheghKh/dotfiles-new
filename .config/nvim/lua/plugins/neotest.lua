return {
	"nvim-neotest/neotest",
	dependencies = {
		"nvim-lua/plenary.nvim",
		"nvim-neotest/nvim-nio",
		"nvim-treesitter/nvim-treesitter",
		"nvim-neotest/neotest-python",
		"nvim-neotest/neotest-go",
		"nvim-neotest/neotest-jest",
		"rouge8/neotest-rust",
		-- "mrcjkb/neotest-haskell",
	},
	lazy = true,
	keys = {
		{ "<leader>tt", "<cmd>lua require('neotest').run.run(vim.fn.expand('%'))<cr>", desc = "Test file" },
		{ "<leader>tT", "<cmd>lua require('neotest').run.run()<cr>", desc = "Test nearest" },
		{
			"<leader>td",
			"<cmd>lua require('neotest').run.run({ vim.fn.expand('%'), strategy = 'dap' })<cr>",
			desc = "Test file dap",
		},
		{ "<leader>tD", "<cmd>lua require('neotest').run.run(trategy = 'dap'})<cr>", desc = "Test nearest dap" },
		{ "<leader>ta", "<cmd>lua require('neotest').run.attach()<cr>", desc = "Attach" },
		{
			"<leader>to",
			"<cmd>lua require('neotest').output.open({enter = true, last_run = true, auto_close = true})<cr>",
			desc = "Output",
		},
		{ "<leader>tO", "<cmd>lua require('neotest').output_panel.toggle()<cr>", desc = "Output panel" },
		{ "<leader>tu", "<cmd>lua require('neotest').summary.toggle()<cr>", desc = "Summary" },
		{ "<leader>tS", "<cmd>lua require('neotest').run.stop()<cr>", desc = "Stop" },
	},
	config = function()
		require("neotest").setup({
			adapters = {
				require("neotest-python")({
					dap = { justMyCode = false },
				}),
				require("neotest-go"),
				require("neotest-jest"),
				-- require("neotest-jest")({
				-- 	jestCommand = "yarn test:local",
				-- 	jestConfigFile = "custom.jest.config.ts",
				-- 	env = { CI = true },
				-- 	cwd = function(path)
				-- 		return vim.fn.getcwd()
				-- 	end,
				-- }),
				-- require("neotest-rust"),
				require("rustaceanvim.neotest"),
				-- require("neotest-haskell"),
			},
			consumers = {
				require("neotest.consumers.overseer"),
			},
		})
	end,
}
