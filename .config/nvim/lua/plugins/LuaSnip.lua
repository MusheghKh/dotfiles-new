return {
	"L3MON4D3/LuaSnip",
		-- follow latest release.
	version = "v2.*",
	-- install jsregexp (optional!).
	build = "make install_jsregexp",
	dependencies = {
		-- "honza/vim-snippets"
		"rafamadriz/friendly-snippets"
	},
	lazy = true,
	config = function()
		-- require("luasnip.loaders.from_vscode").lazy_load()
		-- require("luasnip.loaders.from_snipmate").lazy_load()
		require("luasnip.loaders.from_vscode").lazy_load()
	end,
}
