return {
	"OXY2DEV/markview.nvim",
	-- lazy = false, -- Recommended
	lazy = true,
	ft = "markdown", -- If you decide to lazy-load anyway
	dependencies = {
		"nvim-treesitter/nvim-treesitter",
		"nvim-tree/nvim-web-devicons",
	},
	opts = {
		preview = {
			hybrid_modes = { "n" },
		},
	},
}
