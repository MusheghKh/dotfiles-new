return {
	"kylechui/nvim-surround",
	version = "*",
	lazy = true,
	keys = {
		{ "S", mode = "x" },
		{ "ds" },
		{ "cs" },
		{ "ys" }
	},
	opts = {

	}
}
