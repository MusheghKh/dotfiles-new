local _M = {}

function _M.shallow_copy(t)
	local t2 = {}
	for k, v in pairs(t) do
		t2[k] = v
	end
	return t2
end

function _M.remove_value(table, val)
	for key, value in pairs(table) do
		if value == val then
			table[key] = nil
			return table
		end
	end
	return table
end

function _M.smart_paste()
	--- { row, col }
	local cursor = vim.api.nvim_win_get_cursor(0)
	local line = vim.fn.getline(".")
	local length = line:len()
	if cursor[2] == length then
		-- move cursor one col left to not paste on new line character
		vim.api.nvim_win_set_cursor(0, { cursor[1], cursor[2] - 1 })
		vim.api.nvim_input('"_dp')
	elseif cursor[2] == length - 1 then
		vim.api.nvim_input('"_dp')
	else
		vim.api.nvim_input('"_dP')
	end
end

function _M.smart_paste2()
	local last = vim.fn.getreg("+")
	vim.api.nvim_feedkeys("p", "n", false)

	vim.api.nvim_create_augroup("User_after_paste", { clear = true })
	vim.api.nvim_create_autocmd("TextChanged", {
		group = "User_after_paste",
		callback = function()
			vim.fn.setreg('"', last)
			vim.fn.setreg("+", last)
			vim.api.nvim_del_augroup_by_name("User_after_paste")
		end,
	})
end

function _M.cd_to_git_project()
	local command = [[fd --hidden -c never -a -t d -p --no-ignore-vcs ".*\.git$" ~/Code | while IFS= read -r path; do
		p="${path%/.git/}"
			p="${p#"$HOME/"}"
				echo "$p"
				done]]
	local handle = io.popen(command)
	-- local result = handle:read("*a")
	local result = {}
	for line in handle:lines() do
		table.insert(result, line)
	end
	handle:close()

	vim.ui.select(result, {
		prompt = "Select Project:",
	}, function(choice)
		if choice ~= nil and choice ~= "" then
			vim.print("cd " .. os.getenv("HOME") .. "/" .. choice)
			vim.cmd("cd " .. os.getenv("HOME") .. "/" .. choice)
		end
	end)
end

-- function COPY_CURRENT_BUFNAME()
-- 	local result = vim.api.nvim_buf_get_name(0)
-- 	vim.fn.setreg('"', result)
-- 	vim.fn.setreg('+', result)
-- end

-- function COPY_CURRENT_BUFNAME_BASENAME()
-- 	local result = vim.fs.basename(vim.api.nvim_buf_get_name(0))
-- 	vim.fn.setreg('"', result)
-- 	vim.fn.setreg('+', result)
-- end

--- Jump to window, if not more windows then jump to tmux pane
---@param direction string in form of h,j,k,l
function _M.tmux_move(direction)
	local wnr = vim.fn.winnr()
	vim.cmd("wincmd " .. direction)

	if wnr == vim.fn.winnr() then
		vim.fn.system("tmux select-pane -" .. vim.fn.tr(direction, "phjkl", "lLDUR"))
	end
end

return _M
