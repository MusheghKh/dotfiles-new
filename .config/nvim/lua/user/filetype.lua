local detect_ansible = function(path, bufnr)
	local dir = path:match("(.*/)")
	local lint_file = dir .. ".ansible-lint"
	if vim.fn.filereadable(lint_file) ~= 0 then
		return "yaml.ansible"
	end
	return "yaml"
end

local detect_c_header = function(path, bufnr)
	local lines = vim.api.nvim_buf_get_lines(bufnr, 0, 99, false)
	for _, line in pairs(lines) do
		if string.match(line, "#include <.*%.h>") ~= nil then
			return "c"
		end
		if string.match(line, '#include ".*%.h"') ~= nil then
			return "c"
		end
	end
	return "cpp"
end

vim.filetype.add({
	extension = {
		tfstate = "json",
		jq = "jq",
		-- h = detect_c_header,
		yaml = detect_ansible,
		yml = detect_ansible,
		http = "http",
    tf = "terraform"
	},
	filename = {
		[".ansible-lint"] = "yaml",
		[".dockerignore"] = "gitignore",
		[".shellcheckrc"] = "conf",
	},
	pattern = {
		["/home/.*%.kube/config"] = "yaml",

		[".*%.gitignore.*"] = "gitignore",
		[".*Dockerfile.*"] = "dockerfile",
		[".*Containerfile*"] = "dockerfile",
		[".*docker-compose.ya?ml"] = "yaml.docker-compose",
		[".*Jenkinsfile.*"] = "groovy",

		-- Similar logic to pearofducks/ansible-vim
		[".*group_vars/.*"] = "yaml.ansible",
		[".*host_vars/.*"] = "yaml.ansible",

		[".*handlers/.*%.ya?ml"] = "yaml.ansible",
		[".*roles/.*%.ya?ml"] = "yaml.ansible",
		[".*tasks/.*%.ya?ml"] = "yaml.ansible",

		[".*hosts.*%.ya?ml"] = "yaml.ansible",
		[".*playbook.*%.ya?ml"] = "yaml.ansible",
		[".*main%.ya?ml"] = "yaml.ansible",
		[".*site%.ya?ml"] = "yaml.ansible",
	},
})
