require("user.base")
require("user.mappings")
require("user.filetype")

require("user.def_plugins")

vim.cmd.colorscheme("catppuccin")

require("user.external")
