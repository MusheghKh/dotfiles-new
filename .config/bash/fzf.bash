# Send Generic signal
fzf_sig() {

	(($#)) || {
		echo "signal not specified"
		return 1
	}

	local pid_col
	if [[ $(uname) == Linux ]]; then
		pid_col=2
	elif [[ $(uname) == Darwin ]]; then
		pid_col=3
	else
		echo 'Error: unknown platform'
		return
	fi

	local pids
	pids=$(
		ps -f -u "$USER" | sed 1d | fzf --multi | tr -s '[:blank:]' | cut -d' ' -f"$pid_col"
	)

	if [[ -n $pids ]]; then
		echo "$pids" | xargs kill "$@"
	fi
}

alias fsig="fzf_sig"

# Send SIGHUP
fzf_hup() {
	fzf_sig "-HUP"
}

alias fhup="fzf_hup"

# Send SIGTERM
fzf_term() {
	fzf_sig "-TERM"
}

alias fterm="fzf_term"

# Send SIGINT
fzf_int() {
	fzf_sig "-INT"
}

alias fint="fzf_int"

# Get pids of selected processes
fzf_pgrep() {
	local pid_col
	if [[ $(uname) == Linux ]]; then
		pid_col=2
	elif [[ $(uname) == Darwin ]]; then
		pid_col=3
	else
		echo 'Error: unknown platform'
		return
	fi

	local pids
	pids=$(
		ps -f -u "$USER" | sed 1d | fzf --multi | tr -s '[:blank:]' | cut -d' ' -f"$pid_col"
	)

	if [ -n "$pids" ]; then
		echo "$pids"
	fi
}

alias fpgrep="fzf_pgrep"

# Search Man pages
fzf_man_search() {
	man -k . | fzf | awk '{print $1}' | xargs -r -I {} man {}
}
alias fman="fzf_man_search"

# Search in ~/.ssh/known_hosts
fzf_ssh_host() {
	local host=""
	host="$(cut -d' ' -f 1 ~/.ssh/known_hosts | sort | uniq | fzf)"

	[ -n "$host" ] || return

	# printf "%s" "$host" | xclip -i -f -sel clipboard | xclip -i -sel primary
	printf "%s" "$host" | wl-copy -n
	echo "$host"
}

alias fhost='fzf_ssh_host'

export FZF_CTRL_T_COMMAND=
export FZF_ALT_C_COMMAND=

# if which fd >>/dev/null 2>&1; then
# 	export FZF_DEFAULT_COMMAND="fd -u --type f --color=auto"
# 	# export FZF_DEFAULT_OPTS="--height 75% --multi --reverse --bind ctrl-f:page-down,ctrl-b:page-up $CatppuccinMacchiato"
# 	export FZF_DEFAULT_OPTS="--height 75% --multi --reverse --bind ctrl-f:page-down,ctrl-b:page-up"
# 	export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"
# 	# export FZF_CTRL_T_OPTS="--preview 'bat --color=auto --line-range :500 {}'"
# 	# export FZF_CTRL_T_OPTS="--preview-window down --preview 'if file {} | grep -i 'text'; then bat --color=auto --line-range :500 {}; fi'"
# 	export FZF_CTRL_T_OPTS="--preview-window right --preview 'pistol {}'"
# 	# export FZF_CTRL_T_OPTS="--preview 'head -$LINES {}'"
# 	export FZF_ALT_C_COMMAND='fd -u --type d . --color=auto'
# 	export FZF_ALT_C_OPTS="--preview-window right --preview 'tree -C {} | head -100'"
# else
# 	export FZF_DEFAULT_OPTS="--height 75% --multi --reverse --bind ctrl-f:page-down,ctrl-b:page-up"
# 	export FZF_CTRL_T_OPTS="--preview-window right --preview 'file {}'"
# 	export FZF_ALT_C_OPTS="--preview-window right --preview 'tree -L 4 -C {} | head -100'"
# fi
