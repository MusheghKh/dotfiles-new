# USAGE:
# gen_str 40 to generate string with length 40
# gen_str to generate string with default value
gen_str() {
	tr -dc A-Za-z0-9 </dev/urandom | head -c "${1:-13}"
	echo ''
}

# lf() {
# 	local LAST_DIR_FILE
# 	LAST_DIR_FILE="$(mktemp -t tmp.XXXXXX)"
#
# 	if [ -z "${START_CWD:+x}" ]; then
# 		START_CWD="$PWD"
# 	fi
#
# 	(
# 		trap "test -e $LAST_DIR_FILE && rm -f -- $LAST_DIR_FILE; exit" TERM
# 		trap "test -e $LAST_DIR_FILE && rm -f -- $LAST_DIR_FILE; exit" INT
# 		trap "test -e $LAST_DIR_FILE && rm -f -- $LAST_DIR_FILE; exit" HUP
#
# 		START_CWD="$START_CWD" LAST_DIR_FILE="$LAST_DIR_FILE" command lf "$@"
# 	)
#
# 	last_dir="$(cat "$LAST_DIR_FILE")"
#
# 	if [ -n "$last_dir" ]; then
# 		cd "$last_dir" || exit
# 	fi
# 	command rm -f -- "$LAST_DIR_FILE" 2>/dev/null
# }

lf() {
  echo "Get used to yazi!!!"
}

y() {
  export START_CWD="$PWD"
  local tmp
	tmp="$(mktemp -t "yazi-cwd.XXXXXX")"
	yazi "$@" --cwd-file="$tmp"
	if cwd="$(cat -- "$tmp")" && [ -n "$cwd" ] && [ "$cwd" != "$PWD" ]; then
		builtin cd -- "$cwd" || exit 1
	fi
	rm -f -- "$tmp"
}

vim() {
	if which rg &>/dev/null && which fd &>/dev/null; then
		command vim "$@"
	else
		FZF_DEFAULT_COMMAND="" FZF_DEFAULT_OPTS="" command vim "$@"
	fi
}

nvim() {
	subcommand="$1"

	if [ "$subcommand" = "-help" ]; then
		echo "    $0 function help"
		echo "    $0 -force      -   delete existing server if needed (default)"
		echo "    $0 -random     -   generate random server name no matter what"
		echo "    $0 -noserver   -   launch nvim without server"
		echo "    $0             -   create named server if in git repo otherwise generate random server. If it is a git repo and server already exists, generate random one"
		return 1
	fi

	local server_name
	case "$subcommand" in
		-random)
			shift 1
			server_name="$(gen_str 12)"
			;;
		-noserver)
			shift 1
			command nvim "$@"
			return "$?"
			;;
		-force)
			shift 1
			if [ -d "$PWD/.git" ]; then
				server_name="$(echo "$PWD" | sed 's/\//%/g')"
			else
				server_name="$(gen_str 12)"
			fi
			server_path="$HOME/.cache/nvim_servers/$server_name"
			[ -e "$server_path" ] && {
				rm "$server_path"
			}
			;;
		*)
			if [ -d "$PWD/.git" ]; then
				server_name="$(echo "$PWD" | sed 's/\//%/g')"
			else
				server_name="$(gen_str 12)"
			fi
      [ "$MY_NVIM_LISTEN_ADDRESS" = "$server_name" ] && server_name="$(gen_str 12)"
			server_path="$HOME/.cache/nvim_servers/$server_name"
			[ -e "$server_path" ] && {
				rm "$server_path"
			}
			;;
	esac
	server_path="$HOME/.cache/nvim_servers/$server_name"

	if [ ! -e "$server_path" ]; then
		# NOTE: NVIM_LISTEN_ADDRESS got deleted by nvim
		MY_NVIM_LISTEN_ADDRESS="$server_path" command nvim --listen "$server_path" "$@"
	else
		echo "$server_path already exist. Use another name" 1>&2
	fi
}

load_kubeconfigs() {
	if [ -d "$HOME/.kube/multiconf" ]; then
		KUBECONFIG=""
		while IFS= read -r line; do
			KUBECONFIG="$line${KUBECONFIG:+:$KUBECONFIG}"
		done < <(find "$HOME/.kube/multiconf" -maxdepth 1 -type f -name "*.yml" -o -name "*.yaml")
		export KUBECONFIG
	fi
}
