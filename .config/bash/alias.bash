alias beep='echo -en "\007"'

alias ls="ls --color=auto"
alias ll="ls -ahl --color=auto"
alias grep="grep --color=auto"
# alias ip="ip --color=auto"

alias aws='docker run --rm -ti -v ~/.aws:/root/.aws -v $(pwd):/aws amazon/aws-cli'

alias tty-clock="tty-clock -s -c -C 6 -D"
alias cmatrix="cmatrix -u 2 -B"

alias lg="lazygit"
alias ngg="nvim +Neogit"
alias glo="git log --oneline"

alias tcd='cd `tss path`'
alias za="zoxide add"

alias dbox="distrobox"
alias toqr="qrencode -t ansiutf8"

alias ytap="yt-dlp --extract-audio --audio-format mp3 --audio-quality 0"
alias yta="yt-dlp --extract-audio --audio-format mp3 --audio-quality 0 --no-playlist"
alias ytp="yt-dlp --format \"bv*[ext=mp4]+ba[ext=m4a]/b[ext=mp4]\""
alias yt="yt-dlp --format \"bv*[ext=mp4]+ba[ext=m4a]/b[ext=mp4]\" --no-playlist"

alias k="kubectl"
alias kls="kubectl config get-contexts"
alias kcc="kubie ctx"
alias kns="kubie ns"
alias kind="KUBECONFIG=~/.kube/kind.yml kind"
alias minikube="KUBECONFIG=~/.kube/minikube.yml minikube"

alias batl="bat --paging=auto"

alias get_idf='source ~/.esp/export.sh'

alias poetry-activate='source $(poetry env info --path)/bin/activate'
