# node version manager
export NVM_DIR="$HOME/.nvm"
# Go path
export GOPATH="$HOME/.go"
# Turn off cowsay in ansible
export ANSIBLE_NOCOWS="True"

# default editor
if [ -n "${SSH_CLIENT:+x}" ] || [ -n "${SSH_CONNECTION:+x}" ] || [ -n "${SSH_TTY:+x}" ]; then
	# ssh session
	export EDITOR="vim"
else
	# local session
	export EDITOR="nvim"
fi
export SUDO_EDITOR="vim"

# default terminal
export TERMINAL="wezterm"

# nohup.out file
export NHO="$HOME/.nohup.out"

# zk
export ZK_NOTEBOOK_DIR="$HOME/zk"

# zoxide (echo CDed path)
export _ZO_ECHO=1

# PATH variable
export_yarn_gloabal_bin() {
	if command -v yarn &>/dev/null; then
		local yarn_global_bin
		yarn_global_bin=$(yarn global bin 2>>/dev/null)
		if [ -d "$yarn_global_bin" ] && ! echo "$PATH" | grep -q "$yarn_global_bin"; then
			export PATH="$yarn_global_bin${PATH:+:$PATH}"
		fi
	fi
}

export_yarn_gloabal_bin
unset -f export_yarn_gloabal_bin

export_gem_home() {
	if command -v ruby &>/dev/null; then
		GEM_HOME="$(ruby -e 'puts Gem.user_dir' 2>/dev/null)"
		export GEM_HOME
		if [ -d "$GEM_HOME" ] && ! echo "$PATH" | grep -q "$GEM_HOME"; then
			export PATH="$GEM_HOME/bin${PATH:+:$PATH}"
		fi
	fi
}

export_gem_home
unset -f export_gem_home

export_perl_base() {
	export PERL_BASE="$HOME/.perl5"
	export PERL_MM_OPT="INSTALL_BASE=$PERL_BASE"
	export PERL_MB_OPT="--install_base $PERL_BASE"
	export PERL5LIB="$PERL_BASE/lib/perl5"
	# export MANPATH="$PERL_BASE/man${MANPATH:+:$MANPATH}"
	if ! echo "$PATH" | grep -q "$PERL_BASE/bin"; then
		export PATH="$PERL_BASE/bin${PATH:+:$PATH}"
	fi
}

export_perl_base
unset -f export_perl_base

if [ -d "$HOME/.emacs.d/bin" ] && ! echo "$PATH" | grep -q "$HOME"/.emacs.d/bin; then
	export PATH="$HOME/.emacs.d/bin${PATH:+:$PATH}"
fi

if [ -d "$HOME/.local/bin" ] && ! echo "$PATH" | grep -q "$HOME"/.local/bin; then
	export PATH="$HOME/.local/bin${PATH:+:$PATH}"
fi

if [ -d "$HOME/bin" ] && ! echo "$PATH" | grep -q "$HOME"/bin; then
	export PATH="$HOME/bin${PATH:+:$PATH}"
fi

if [ -d "/var/lib/flatpak/exports/bin" ] && ! echo "$PATH" | grep -q /var/lib/flatpak/exports/bin; then
	export PATH="/var/lib/flatpak/exports/bin${PATH:+:$PATH}"
fi

if [ -d "$GOPATH/bin" ] && ! echo "$PATH" | grep -q "$GOPATH"/bin; then
	export PATH="$GOPATH/bin${PATH:+:$PATH}"
fi

if [ -d "$HOME/.cargo/bin" ] && ! echo "$PATH" | grep -q "$HOME"/.cargo/bin; then
	export PATH="$HOME/.cargo/bin${PATH:+:$PATH}"
fi

if [ -d "${KREW_ROOT:-$HOME/.krew}/bin" ] && ! echo "$PATH" | grep -q "${KREW_ROOT:-$HOME/.krew}"/bin; then
	export PATH="${KREW_ROOT:-$HOME/.krew}/bin${PATH:+:$PATH}"
fi

if [ -d "$HOME/.minio-binaries" ] && ! echo "$PATH" | grep -q "$HOME"/.minio-binaries; then
	export PATH="$HOME/.minio-binaries${PATH:+:$PATH}"
fi
