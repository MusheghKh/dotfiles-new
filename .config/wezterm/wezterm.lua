local wezterm = require("wezterm")
local action = wezterm.action
-- local mux = wezterm.mux

local config = {}

if wezterm.config_builder then
	config = wezterm.config_builder()
end

local opacity = 0.9

-- Appearance
config.color_scheme = "Catppuccin Macchiato"
-- config.color_scheme = "Tokyo Night Moon"
-- config.color_scheme = "OneDark (base16)"
-- config.color_scheme = 'Hardcore'
-- config.color_scheme = "Tango (terminal.sexy)"
-- config.color_scheme = 'Brewer (dark) (terminal.sexy)'
-- config.color_scheme = 'kanagawa (Gogh)'

config.window_background_opacity = opacity
config.font = wezterm.font("Fira Code")
-- config.dpi = 96.0
-- config.line_height = 1.1
config.font_size = 11
-- config.cell_width = 1
config.audible_bell = "Disabled"
-- config.exit_behavior = "Close"
config.hide_tab_bar_if_only_one_tab = true
config.inactive_pane_hsb = {
	saturation = 0.8,
	brightness = 0.7,
}
config.scrollback_lines = 10000
config.window_close_confirmation = "NeverPrompt"
config.initial_cols = 130
config.initial_rows = 35

wezterm.on("toggle-ligature", function(window, pane)
	local overrides = window:get_config_overrides() or {}
	if not overrides.harfbuzz_features then
		-- If we haven't overridden it yet, then override with ligatures disabled
		overrides.harfbuzz_features = { "calt=0", "clig=0", "liga=0" }
	else
		-- else we did already, and we should disable out override now
		overrides.harfbuzz_features = nil
	end
	window:set_config_overrides(overrides)
end)

wezterm.on("toggle-opacity", function(window, pane)
	local overrides = window:get_config_overrides() or {}
	if not overrides.window_background_opacity then
		overrides.window_background_opacity = 1
	else
		overrides.window_background_opacity = nil
	end
	window:set_config_overrides(overrides)
end)

-- nvim zen mode allow change font size
wezterm.on('user-var-changed', function(window, pane, name, value)
    local overrides = window:get_config_overrides() or {}
    if name == "ZEN_MODE" then
        local incremental = value:find("+")
        local number_value = tonumber(value)
        if incremental ~= nil then
            while (number_value > 0) do
                window:perform_action(wezterm.action.IncreaseFontSize, pane)
                number_value = number_value - 1
            end
            overrides.enable_tab_bar = false
        elseif number_value < 0 then
            window:perform_action(wezterm.action.ResetFontSize, pane)
            overrides.font_size = nil
            overrides.enable_tab_bar = true
        else
            overrides.font_size = number_value
            overrides.enable_tab_bar = false
        end
    end
    window:set_config_overrides(overrides)
end)

-- wezterm.on("gui-startup", function(cmd)
-- 	local tab, pane, window = mux.spawn_window(cmd or {})
-- 	window:gui_window():maximize()
-- end)

-- wezterm.on("gui-attached", function(domain)
-- 	-- maximize all displayed windows on startup
-- 	local workspace = mux.get_active_workspace()
-- 	for _, window in ipairs(mux.all_windows()) do
-- 		if window:get_workspace() == workspace then
-- 			window:gui_window():maximize()
-- 		end
-- 	end
-- end)

-- Mouse
config.mouse_bindings = {
	-- Change the default click behavior so that it populates
	-- the Clipboard rather the PrimarySelection.
	{
		event = { Up = { streak = 1, button = "Left" } },
		mods = "CTRL",
		action = action.OpenLinkAtMouseCursor,
	},
}

-- Keys
config.leader = { key = "t", mods = "CTRL", timeout_milliseconds = 3000 }
config.keys = {
	-- leader
	{
		key = "t",
		mods = "LEADER|CTRL",
		action = action.SendKey({ key = "t", mods = "CTRL" }),
	},

	-- pane
	{
		key = "s",
		mods = "LEADER",
		action = action.SplitVertical({ domain = "CurrentPaneDomain" }),
	},
	{
		key = "v",
		mods = "LEADER",
		action = action.SplitHorizontal({ domain = "CurrentPaneDomain" }),
	},
	{
		key = "x",
		mods = "LEADER",
		action = action.CloseCurrentPane({ confirm = true }),
	},
	{
		key = "h",
		mods = "CTRL|ALT|SHIFT",
		action = action.ActivatePaneDirection("Left"),
	},
	{
		key = "j",
		mods = "CTRL|ALT|SHIFT",
		action = action.ActivatePaneDirection("Down"),
	},
	{
		key = "k",
		mods = "CTRL|ALT|SHIFT",
		action = action.ActivatePaneDirection("Up"),
	},
	{
		key = "l",
		mods = "CTRL|ALT|SHIFT",
		action = action.ActivatePaneDirection("Right"),
	},

	-- tab

	-- Create a new tab in the same domain as the current pane.
	-- This is usually what you want.
	{
		key = "c",
		mods = "LEADER",
		action = action.SpawnTab("CurrentPaneDomain"),
	},
	{
		key = "a",
		mods = "LEADER",
		action = action.ShowTabNavigator,
	},
	{
		key = "X",
		mods = "LEADER",
		action = action.CloseCurrentTab({ confirm = true }),
	},
	{
		key = "0",
		mods = "CTRL",
		action = action.ResetFontSize,
	},
	{
		key = "T",
		mods = "LEADER",
		action = action.ActivateCommandPalette,
	},
	{
		key = "[",
		mods = "LEADER",
		action = action.ActivateCopyMode,
	},

	--- toggles
	{
		key = "e",
		mods = "LEADER",
		action = action.EmitEvent("toggle-ligature"),
	},
	{
		key = "o",
		mods = "LEADER",
		action = action.EmitEvent("toggle-opacity"),
	},

	{
		key = "q",
		mods = "SUPER",
		action = action.Nop,
	},
}

return config
