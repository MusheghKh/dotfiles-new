require("user.base")
require("user.mappings")

require("user.def_plugins")

vim.cmd.colorscheme "quiet"

require("user.external")
