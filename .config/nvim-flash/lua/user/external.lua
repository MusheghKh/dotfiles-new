USER_EXTERNAL = {}

function USER_EXTERNAL.flash_it(y, x)
	vim.api.nvim_create_augroup("User_flash_it_autogroup", { clear = true })
	vim.api.nvim_create_autocmd("User", {
		group = "User_flash_it_autogroup",
		pattern = "VeryLazy",
		callback = function()
			vim.cmd("silent normal " .. y .. "Gzv" .. x .. "|")
			vim.keymap.set("n", "q", "ZQ", { buffer = 0 })
			-- vim.cmd("Lazy load flash.nvim")
			-- require("flash").jump()
			vim.api.nvim_del_augroup_by_name("User_flash_it_autogroup")
		end,
	})
end

function USER_EXTERNAL.q_to_quit()
	vim.api.nvim_create_augroup("User_flash_it_autogroup", { clear = true })
	vim.api.nvim_create_autocmd("User", {
		group = "User_flash_it_autogroup",
		pattern = "VeryLazy",
		callback = function()
			vim.keymap.set("n", "q", "ZQ", { buffer = 0 })
			vim.api.nvim_del_augroup_by_name("User_flash_it_autogroup")
		end,
	})
end
