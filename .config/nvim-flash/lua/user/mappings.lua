local user_func = require("user.func")

local function set_keymaps()
	-- basic BEGIN
	-- vim.keymap.set("n", "[<leader>", "O<Esc>j", { desc = "Insert line below" })
	-- vim.keymap.set("n", "]<leader>", "o<Esc>k", { desc = "Insert line above" })

	vim.keymap.set("n", "ZZ", "<cmd>wqa<cr>", { desc = "Write and exit" })
	vim.keymap.set("n", "ZQ", "<cmd>qa<cr>", { desc = "Exit" })

	vim.keymap.set("n", "vil", "$v^", { desc = "Select line" })
	vim.keymap.set("n", "yil", "$v^y", { desc = "Yank line" })

	-- vim.keymap.set("v", "<leader>y", '"+y', { desc = "Copy to system clip" })
	-- vim.keymap.set("n", "<leader>yp", COPY_CURRENT_BUFNAME, { desc = "Copy current buf name" })
	-- vim.keymap.set("n", "<leader>yf", COPY_CURRENT_BUFNAME_BASENAME, { desc = "Copy current buf name" })

	--	vim.keymap.set("x", "<leader>p", '"_dp', { desc = "Smart paste (p)" })
	--	vim.keymap.set("x", "<leader>P", '"_dP', { desc = "Smart paste (P)" })
	vim.keymap.set("x", "<leader>p", user_func.smart_paste2, { desc = "Smart paste (word)" })

	vim.keymap.set("n", "Q", "@q", { desc = "Play main macro" })

	vim.keymap.set({ "n", "i", "c", "v" }, "<C-S-V>", "<C-r>+", { desc = "Paste from clipboard" })

	vim.keymap.set("t", "<C-S-V>", "<C-\\><C-N>pi", { desc = "Paste from clipboard" })

	-- run zz after search
	-- vim.keymap.set("n", "n", "nzz")
	-- vim.keymap.set("n", "N", "Nzz")
	-- vim.keymap.set("n", "*", "*zz")
	-- vim.keymap.set("n", "#", "#zz")

	-- run zz after search
	-- vim.keymap.set("c", "<cr>", function()
	-- 	return vim.fn.getcmdtype() == "/" and "<cr>zz" or "<cr>"
	-- end, { expr = true })

	-- vim.keymap.set("n", "<C-d>", "<C-d>zz")
	-- vim.keymap.set("n", "<C-u>", "<C-u>zz")
	-- basic END

	-- Terminal BEGIN
	vim.keymap.set("t", "<C-x>[", "<C-\\><C-n>", { desc = "Exit terminal mode" })
	vim.keymap.set("t", "<C-x><C-x>", "<C-\\><C-n>", { desc = "Exit terminal mode" })
	-- vim.keymap.set("t", "<C-x><C-x>", "<C-\\><C-n>", { desc = "Exit terminal mode" })
	-- vim.keymap.set("t", "<C-w>h", "<C-\\><C-n><C-w>h", { desc = "Window left" })
	-- vim.keymap.set("t", "<C-w>j", "<C-\\><C-n><C-w>j", { desc = "Window down" })
	-- vim.keymap.set("t", "<C-w>k", "<C-\\><C-n><C-w>k", { desc = "Window up" })
	-- vim.keymap.set("t", "<C-w>l", "<C-\\><C-n><C-w>l", { desc = "Window right" })
	-- Terminal END

	-- Windows BEGIN
	vim.keymap.set("n", "<leader>wc", "<C-w>c", { desc = "Close window" })
	vim.keymap.set("n", "<leader>ws", "<C-w>s", { desc = "Horizontal split" })
	vim.keymap.set("n", "<leader>wv", "<C-w>v", { desc = "Vertiacl split" })

	-- vim.keymap.set("n", "<C-h>", "<C-w>h", { desc = "Window left" })
	-- vim.keymap.set("n", "<C-j>", "<C-w>j", { desc = "Window down" })
	-- vim.keymap.set("n", "<C-k>", "<C-w>k", { desc = "Window up" })
	-- vim.keymap.set("n", "<C-l>", "<C-w>l", { desc = "Window right" })

	vim.keymap.set("n", "<M-h>", function()
		user_func.tmux_move("h")
	end, { desc = "Window h" })
	vim.keymap.set("n", "<M-j>", function()
		user_func.tmux_move("j")
	end, { desc = "Window j" })
	vim.keymap.set("n", "<M-k>", function()
		user_func.tmux_move("k")
	end, { desc = "Window k" })
	vim.keymap.set("n", "<M-l>", function()
		user_func.tmux_move("l")
	end, { desc = "Window l" })

	vim.keymap.set("t", "<M-l>", function()
		user_func.tmux_move("h")
	end, { desc = "Window h" })
	vim.keymap.set("t", "<M-j>", function()
		user_func.tmux_move("j")
	end, { desc = "Window j" })
	vim.keymap.set("t", "<M-k>", function()
		user_func.tmux_move("k")
	end, { desc = "Window k" })
	vim.keymap.set("t", "<M-l>", function()
		user_func.tmux_move("l")
	end, { desc = "Window l" })
	-- Windows END

	-- Buffers BEGIN
	vim.keymap.set("n", "<leader>bn", "<cmd>enew<cr>", { desc = "New Buffer" })
	vim.keymap.set("n", "<leader>bw", "<cmd>cd %:p:h | pwd<cr>", { desc = "CWD to current file" })
	vim.keymap.set("n", "<leader>bt", function()
		local filetype = vim.fn.input("Enter FileType: ")
		if string.len(filetype) ~= 0 then
			vim.bo.filetype = filetype
		end
	end, { desc = "Set filetype" })
	-- Buffers END

	-- Selection BEGIN
	vim.keymap.set("s", "<cr>", "a<bs>", { desc = "delete selection" })
	vim.keymap.set("s", "<bs>", "a<bs>", { desc = "delete selection" })
	-- Selection END

	-- Tabs BEGIN
	vim.keymap.set("n", "gh", "<cmd>tabnext<cr>", { desc = "Next Tab" })
	-- Tabs END

	-- Spelling BEGIN
	vim.keymap.set("n", "<leader>ei", function()
		local lang = vim.fn.input("Enter language code: ")
		if string.len(lang) ~= 0 then
			vim.opt.spelllang = lang
		end
	end, { desc = "Set spelllang" })

	vim.keymap.set("n", "<leader>eI", function()
		local lang = vim.fn.input("Enter language code: ")
		if string.len(lang) ~= 0 then
			vim.bo.spelllang = lang
		end
	end, { desc = "Set spelllang (buffer)" })
	-- Spelling END
end

vim.api.nvim_create_augroup("User_keybindings", { clear = true })
vim.api.nvim_create_autocmd({ "User" }, {
	pattern = "VeryLazy",
	group = "User_keybindings",
	callback = set_keymaps,
})

vim.api.nvim_create_autocmd({ "TermEnter" }, {
	group = "User_keybindings",
	callback = function(event, args)
		vim.keymap.set("n", "q", "i", { desc = "Exit normal mode", buffer = event.buf })
	end,
})
