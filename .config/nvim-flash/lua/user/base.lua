vim.g.mapleader = " "
vim.g.maplocalleader = "\\"
-- vim.cmd([[ let maplocalleader = " " ]])

-- vim.wo.winbar = " ..."
-- vim.opt.tabline = "..."
vim.opt.showtabline = 2

-- Show line numbers on the sidebar
vim.opt.number = true
vim.opt.relativenumber = true
-- Autoindent when starting new line, or using `o` or `O`.
vim.opt.autoindent = true
-- Indent using two spaces
vim.opt.tabstop = 2
vim.opt.shiftwidth = 2
-- " Use 'shiftwidth' when using `<Tab>` in front of a line.
-- " By default it's used only for shift commands (`<`, `>`).
vim.opt.smarttab = true
vim.opt.softtabstop = 2

-- Use spaces instead of tabs
vim.opt.expandtab = true

-- disable mouse
-- vim.o.mouse = ""

-- Enable mouse for scrolling and window resizing
vim.opt.mouse = "a"

-- Disable strange Vi defaults.
-- vim.o.nocompatible = true
-- Enable highlighted case-insensitive incremential search
vim.opt.incsearch = true
-- Force utf-8 encoding
vim.opt.encoding = "utf-8"
-- Reload unchanged files automatically
vim.opt.autoread = true
-- Highlight line under cursor. It helps with navigation
vim.opt.cursorline = true
vim.opt.cursorlineopt = "line"
-- Keep 4 lines above or below the cursor when scrolling
-- vim.o.scrolloff = 4
vim.opt.scrolloff = 8
-- disable search when reached to end of file
vim.opt.wrapscan = false
-- Enable search highlighting
vim.opt.hlsearch = true
-- Ignore case when searching
vim.opt.ignorecase = true
-- Show mode in statusbar, not separately
vim.opt.showmode = false
-- " Don't ignore case when search has capital letter
-- " (although also don't ignore case by default).
vim.opt.smartcase = true
-- Set window title by default
vim.opt.title = true
-- Always focus on split window
vim.opt.splitright = true
vim.opt.splitbelow = true

vim.opt.colorcolumn = "100"

-- Disable conceallevel in general
vim.opt.conceallevel = 0
vim.opt.concealcursor = ""

-- If this many milliseconds nothing is typed the swap file will be
vim.opt.updatetime = 1000

-- wrap lines
vim.opt.wrap = true

-- diffsplit prefer vertical split
vim.opt.diffopt:append("vertical")

-- for nvim-tree
-- vim.g.loaded_netrw = 1
-- vim.g.loaded_netrwPlugin = 1

-- folds
vim.opt.foldcolumn = "0"
-- vim.opt.foldlevel = 99 -- Using ufo provider need a large value, feel free to decrease the value
-- vim.opt.foldlevelstart = 99
vim.opt.foldenable = false

-- vim.opt.langmap =
-- 	"щ`,ь-,ъ=,яq,шw,еe,рr,тt,ыy,уu,иi,оo,пp,ю[,ж],аa,сs,дd,фf,гg,чh,йj,кk,лl,зz,хx,цc,вv,бb,нn,мm,Щ~,Ь_,Ъ+,ЯQ,ШW,ЕE,РR,ТT,ЫY,УU,ИI,ОO,ПP,Ю{,Ж},АA,СS,ДD,ФF,ГG,ЧH,ЙJ,КK,ЛL,ЗZ,ХX,ЦC,ВV,БB,НN,МM"

-- Improve displayed color if supported by terminal
vim.cmd([[
if &t_Co >= 256
  set termguicolors
end
]])

-- Auto reload if file was changed somewhere else (for autoread)
vim.cmd([[
augroup auto-reload
autocmd!
autocmd FocusGained,BufEnter,CursorHold,CursorHoldI * if !bufexists("[Command Line]") | checktime | endif
augroup END
]])

-- Подсветить скопированное
vim.api.nvim_create_augroup("yank-highlight", { clear = true })
vim.api.nvim_create_autocmd("TextYankPost", {
	group = "yank-highlight",
	callback = function()
		vim.highlight.on_yank({ higroup = "IncSearch", timeout = 100 })
	end,
})

-- Отключает автокомментирование новой строки
vim.api.nvim_create_augroup("format-options", { clear = true })
vim.api.nvim_create_autocmd("FileType", {
	group = "format-options",
	callback = function()
		vim.opt.formatoptions:remove({ "c", "r", "o" })
	end,
})

-- Убирает подсветку после поиска после ухода со строки
-- local combo = 0
-- local function manage_hlsearch(char)
--   if vim.fn.mode() ~= "n" then
--     return
--   end
--
--   -- local keys = { "<CR>", "n", "N", "*", "#", "?", "/", "z" }
--   local keys = { "n", "N", "*", "#", "?", "/" }
--   local tr_key = vim.fn.keytrans(char)
--
--   if tr_key == "z" then
--     if combo == 0 then
--       return
--     end
--     if combo == 1 then
--       combo = 2
--       return
--     end
--     if combo == 2 then
--       vim.opt.hlsearch = true
--       combo = 0
--       return
--     end
--     print("[ERROR] manage_hlsearch")
--     vim.opt.hlsearch = false
--     combo = 0
--     return
--   end
--
--   if vim.tbl_contains(keys, tr_key) then
--     combo = 1
--     return
--   end
--
--   vim.opt.hlsearch = false
--   combo = 0
-- end
-- local hl_ns = vim.api.nvim_create_namespace("hl_search")
-- vim.on_key(manage_hlsearch, hl_ns)

-- Turn off hlsearch after pressing search keys
local function manage_hlsearch(char)
	if vim.fn.mode() ~= "n" then
		return
	end

	-- local keys = { "<CR>", "n", "N", "*", "#", "?", "/", "z" }
	local keys = { "n", "N", "*", "#", "?", "/" }
	local tr_key = vim.fn.keytrans(char)

	if vim.tbl_contains(keys, tr_key) then
		vim.opt.hlsearch = true
	end
end
local hl_ns = vim.api.nvim_create_namespace("hl_search")
vim.on_key(manage_hlsearch, hl_ns)

vim.api.nvim_create_autocmd("UIEnter", {
	callback = function()
		vim.defer_fn(function()
			vim.api.nvim_exec_autocmds("User", { pattern = "SuperLazy" })
		end, 50) -- 1000 ms (1 second) delay
	end,
})

-- Clipboard
-- Use OSC52 if running in ssh
if vim.env.SSH_CLIENT ~= nil or vim.env.SSH_CONNECTION ~= nil or vim.env.SSH_TTY ~= nil then
	vim.g.clipboard = {
		name = "OSC 52",
		copy = {
			["+"] = require("vim.ui.clipboard.osc52").copy("+"),
			["*"] = require("vim.ui.clipboard.osc52").copy("*"),
		},
		paste = {
			["+"] = require("vim.ui.clipboard.osc52").paste("+"),
			["*"] = require("vim.ui.clipboard.osc52").paste("*"),
		},
	}
	vim.api.nvim_create_autocmd("UIEnter", {
		callback = function()
			vim.cmd([[ set clipboard+=unnamedplus ]])
		end,
	})
elseif vim.fn.has("clipboard") == 1 then
	vim.cmd([[ set clipboard+=unnamedplus ]])
end

-- Neovide
vim.opt.guifont = "Fira Code:h11"
