#!/bin/sh

path=$(realpath "$0")
dir=$(dirname "$path")

cd "$dir" || exit 1

ansible-playbook --ask-become-pass -vv main.yml "$@"
