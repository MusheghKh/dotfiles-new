"""""""""""""""
" vim-oscyank "
"""""""""""""""
if exists(':OSCYankRegister')
			nmap <leader>y <Plug>OSCYankOperator
			nmap <leader>yy <leader>y_
			vmap <leader>y <Plug>OSCYankVisual

			autocmd TextYankPost *
				\ if v:event.operator is 'y' |
				\ execute 'OSCYankRegister "' |
				\ endif
endif
