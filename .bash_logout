# ~/.bash_logout

if [[ $SSH_CLIENT || $SSH_CONNECTION || $SSH_TTY ]]; then
	printf "%s\n%s\n" "2.1.13" "$(gpg --version | head -n 1 | cut -d ' ' -f 3)" | sort --check=quiet --version-sort && {
		agent_socket="$(gpgconf --list-dirs agent-socket 2>/dev/null)"
		agent_ssh_socket="$(gpgconf --list-dirs agent-ssh-socket 2>/dev/null)"
		[ -e "$agent_socket" ] && rm "$agent_socket"
		[ -e "$agent_ssh_socket" ] && rm "$agent_ssh_socket"
	}
fi
