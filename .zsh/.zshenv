# set LS_COLORS
eval $(dircolors --sh 2> /dev/null)

# node version manager
export NVM_DIR="$HOME/.nvm"
# Go path
export GOPATH="$HOME/.go"
# Turn off cowsay in ansible
export ANSIBLE_NOCOWS="True"

# default editor
if [ -n "${SSH_CLIENT:+x}" ] || [ -n "${SSH_CONNECTION:+x}" ] || [ -n "${SSH_TTY:+x}" ]; then
	# ssh session
	export EDITOR="vim"
else
	# local session
	export EDITOR="nvim"
fi
export SUDO_EDITOR="vim"

# default terminal
export TERMINAL="wezterm"

# nohup.out file
export NHO="$HOME/.nohup.out"

# zk
export ZK_NOTEBOOK_DIR="$HOME/zk"

# zoxide (echo CDed path)
export _ZO_ECHO=1

# PATH variable
typeset -U path PATH

if (which yarn &> /dev/null) ; then
	path=($(yarn global bin 2> /dev/null) $path)
fi

path=(~/.emacs.d/bin $path)

path=(~/.local/bin $path)

path=(~/bin $path)

path=(/var/lib/flatpak/exports/bin $path)

path=($GOPATH/bin $path)

path=(~/.cargo/bin $path)

path=(~/.minio-binaries $path)

export PERL_BASE="$HOME/.perl5"
export PERL_MM_OPT="INSTALL_BASE=$PERL_BASE"
export PERL_MB_OPT="--install_base $PERL_BASE"
export PERL5LIB="$PERL_BASE/lib/perl5"
# export MANPATH="$PERL_BASE/man${MANPATH:+:$MANPATH}"
path=($PERL_BASE/bin $path)

if (which ruby &> /dev/null) ; then
	export GEM_HOME="$(ruby -e 'puts Gem.user_dir' 2> /dev/null)"
	path=("$GEM_HOME/bin" $path)
fi

path=("${KREW_ROOT:-$HOME/.krew}/bin" $path)

export PATH
