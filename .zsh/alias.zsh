[ -s "$HOME/.config/bash/alias.bash" ] && source "$HOME/.config/bash/alias.bash" || true

# for history file format
alias history='fc -il 1'
