# Enable colors
autoload -U colors && colors

#################
# PS1 functions #
#################
# check_root() {
#     if [ $(id -u) -eq 0 ]
#     then
#         echo "%B%{$fg[yellow]%}#%b"
#     else
#         echo "%B%{$fg[yellow]%}\$%b"
#     fi
# }
#
# check_last_exit_code() {
# 		local LAST_EXIT_CODE=$?
# 		if [ $LAST_EXIT_CODE -ne 0 ]; then
# 				echo "%B%{$fg[red]%}$LAST_EXIT_CODE%b"
# 		else
# 				echo "%B%{$fg[green]%}$LAST_EXIT_CODE%b"
# 		fi
# }

# # disables prompt mangling in virtual_env/bin/activate
# export VIRTUAL_ENV_DISABLE_PROMPT=1
#
# show_venvs() {
#   result=""
# 	if [ -n "$VIRTUAL_ENV" ]; then
# 		result=" %{$fg[green]%}python(${VIRTUAL_ENV:t:gs/%/%%})"
# 	fi
#
#   if [ -n "$CONDA_DEFAULT_ENV" ]; then
# 		result="${result} %{$fg[green]%}conda(${CONDA_DEFAULT_ENV:t:gs/%/%%})"
#   fi
#   echo "$result"
# }

# show_git_branch() {
#     local P_GIT=""
#     # check is pwd is git repo
#     if [ -d "$PWD/.git" ] ; then
#         if (which git &> /dev/null) ; then
#             # check if there is uncommitted changes
#             if [ $(git --git-dir="$PWD/.git" --work-tree="$PWD" status --porcelain | wc -l) -ne 0 ] ; then
#                 P_GIT=" %{$fg[red]%}git($(git branch --show-current))"
#             else
#                 # check if branch has upstream
#                 if [ $(git branch -v | wc -l) -eq 0 ] ; then
#                     P_GIT=" %{$fg[yellow]%}git($(git branch --show-current))"
#                 else
#                     # check if there is unpushed changes
#                     if (git branch -v | grep -E -q "\* .* .* \[.*\] .*") ; then
#                         P_GIT=" %{$fg[yellow]%}git($(git branch --show-current))"
#                     else
#                         P_GIT=" %{$fg[green]%}git($(git branch --show-current))"
#                     fi
#                 fi
#             fi
#         fi
#     fi
#     echo "$P_GIT"
# }

# # Initial PS1
# PS1=""
#
# if [ -n "${SSH_CLIENT:+x}" ] || [ -n "${SSH_CONNECTION:+x}" ] || [ -n "${SSH_TTY:+x}" ]; then
# 	# ssh connection
# 	# $1=success of last command
# 	set_prompt () {
# 			local success="$1"
# 			PS1="%{$fg[yellow]%}%n%b@%{$fg[yellow]%}%m%b:%{$fg[blue]%}%~%b [${success} I] $ "
# 	}
# else
# 	# local session
# 	# $1=success of last command
# 	set_prompt () {
# 			local success="$1"
# 			local newline=$'\n'
# 			local date=" %{$fg[yellow]%}$(date +%H:%M:%S)"
# 			local left="[%{$fg[blue]%}%n%B%{$fg[yellow]%}@%b%{$fg[red]%}%m%B%{$fg[yellow]%}:%b%{$fg[blue]%}%~%b] [ $success$(show_git_branch)$(show_venvs)$date%{$reset_color%} ] |"
# 			local second_line="$(check_root) "
# 			PS1="${left}${newline}${second_line}"
# 	}
# fi
#
# precmd() {
# 	set_prompt "$(check_last_exit_code)"
# }

# History in cache directory:
HISTSIZE=10000
SAVEHIST=10000
HISTFILE=~/.zsh/.zhistory

# create a zkbd compatible hash;
# to add other keys to this hash, see: man 5 terminfo
typeset -g -A key

key[Home]="${terminfo[khome]}"
key[End]="${terminfo[kend]}"
key[Insert]="${terminfo[kich1]}"
key[Backspace]="${terminfo[kbs]}"
key[Delete]="${terminfo[kdch1]}"
key[Up]="${terminfo[kcuu1]}"
key[Down]="${terminfo[kcud1]}"
key[Left]="${terminfo[kcub1]}"
key[Right]="${terminfo[kcuf1]}"
key[PageUp]="${terminfo[kpp]}"
key[PageDown]="${terminfo[knp]}"
key[Shift-Tab]="${terminfo[kcbt]}"
key[Control-Left]="${terminfo[kLFT5]}"
key[Control-Right]="${terminfo[kRIT5]}"

# setup key accordingly
[[ -n "${key[Home]}"      ]] && bindkey -- "${key[Home]}"       beginning-of-line
[[ -n "${key[End]}"       ]] && bindkey -- "${key[End]}"        end-of-line
[[ -n "${key[Insert]}"    ]] && bindkey -- "${key[Insert]}"     overwrite-mode
[[ -n "${key[Backspace]}" ]] && bindkey -- "${key[Backspace]}"  backward-delete-char
[[ -n "${key[Delete]}"    ]] && bindkey -- "${key[Delete]}"     delete-char
[[ -n "${key[Up]}"        ]] && bindkey -- "${key[Up]}"         up-line-or-history
[[ -n "${key[Down]}"      ]] && bindkey -- "${key[Down]}"       down-line-or-history
[[ -n "${key[Left]}"      ]] && bindkey -- "${key[Left]}"       backward-char
[[ -n "${key[Right]}"     ]] && bindkey -- "${key[Right]}"      forward-char
[[ -n "${key[PageUp]}"    ]] && bindkey -- "${key[PageUp]}"     beginning-of-buffer-or-history
[[ -n "${key[PageDown]}"  ]] && bindkey -- "${key[PageDown]}"   end-of-buffer-or-history
[[ -n "${key[Shift-Tab]}" ]] && bindkey -- "${key[Shift-Tab]}"  reverse-menu-complete
[[ -n "${key[Control-Left]}"  ]] && bindkey -- "${key[Control-Left]}"  backward-word
[[ -n "${key[Control-Right]}" ]] && bindkey -- "${key[Control-Right]}" forward-word

# Finally, make sure the terminal is in application mode, when zle is
# active. Only then are the values from $terminfo valid.
if (( ${+terminfo[smkx]} && ${+terminfo[rmkx]} )); then
    autoload -Uz add-zle-hook-widget
    function zle_application_mode_start { echoti smkx }
    function zle_application_mode_stop { echoti rmkx }
    add-zle-hook-widget -Uz zle-line-init zle_application_mode_start
    add-zle-hook-widget -Uz zle-line-finish zle_application_mode_stop
fi

############################
# Basic auto/tab complete: #
############################
# When a directory is completed, add a trailing slash instead of a space.
setopt AUTO_PARAM_SLASH
autoload -U compinit
zstyle ':completion:*' menu select interactive
zstyle ':completion:*:warnings' format ' %F{yellow}-- no matches found --%f'
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}
zmodload zsh/complist
compinit
_comp_options+=(globdots)		# Include hidden files.

###########
# vi mode #
###########
bindkey -v
export KEYTIMEOUT=1

# Change prompt when vi mode is changes
# function zle-line-init zle-keymap-select {
#     case $KEYMAP in
#         vicmd)
# 						PS1="$(sed "s/|$/|NORMAL/" <<< $PS1)"
# 						PS1="$(sed 's/I] \$ $/N] $ /' <<< $PS1)"
#             ;;
#         viins|main)
# 						PS1="$(sed "s/|NORMAL$/|/" <<< $PS1)"
# 						PS1="$(sed 's/N] \$ $/I] $ /' <<< $PS1)"
#             ;;
#     esac
#     zle reset-prompt
# }
# zle -N zle-line-init
# zle -N zle-keymap-select

##############
# emacs mode #
##############
# bindkey -e

# Use vim keys in tab complete menu:
bindkey -M menuselect '^h' vi-backward-char
bindkey -M menuselect '^k' vi-up-line-or-history
bindkey -M menuselect '^l' vi-forward-char
bindkey -M menuselect '^j' vi-down-line-or-history
# bindkey -v '^?' backward-delete-char

# Edit line in vim with ctrl-e:
autoload edit-command-line; zle -N edit-command-line
bindkey '^e' edit-command-line

# aliases
test -s ~/.zsh/alias.zsh && source ~/.zsh/alias.zsh || true
test -s ~/.zsh/functions.zsh && source ~/.zsh/functions.zsh || true
test -s ~/.zsh/fzf.zsh && source ~/.zsh/fzf.zsh || true

# use gpg-agent instead of ssh-agent
unset SSH_AGENT_PID
if [ "${gnupg_SSH_AUTH_SOCK_by:-0}" -ne $$ ]; then
  SSH_AUTH_SOCK="$(gpgconf --list-dirs agent-ssh-socket 2>/dev/null)"
	export SSH_AUTH_SOCK
fi

# gpg tty problem see Arch wiki GnuPG
GPG_TTY=$(tty)
export GPG_TTY
if pgrep -u "$USER" gpg-agent >/dev/null 2>&1; then
	gpg-connect-agent updatestartuptty /bye >/dev/null
fi

# This loads node version manager
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" || true

# ghcup (Haskell)
[ -s ~/.ghcup/env ] && source ~/.ghcup/env || true # ghcup-env

# fzf
# global install
which fzf >/dev/null 2>&1 && source <(fzf --zsh)
# local install
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh || true

# ZSH plugins
[ -s ~/.zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh ] && source ~/.zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh || true
[ -s ~/.zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh ] && source ~/.zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh || true

load_kubeconfigs

which zoxide >/dev/null 2>&1 && eval "$(zoxide init zsh)" || true

which direnv >/dev/null 2>&1 && eval "$(direnv hook zsh)" || true

which starship >/dev/null 2>&1 && eval "$(starship init zsh)" || PS1='%F{blue}%n@%m%f %F{green}%/%f %F{yellow}%\$%f ' # david@macbook /tmp $ 

test -d ~/.miniforge3 && eval "$(~/.miniforge3/bin/conda shell.zsh hook)" || true

export PYENV_ROOT="$HOME/.pyenv"
# [[ -d $PYENV_ROOT/bin ]] && export PATH="$PYENV_ROOT/bin:$PATH"
which pyenv >/dev/null 2>&1 && eval "$(pyenv init -)"
