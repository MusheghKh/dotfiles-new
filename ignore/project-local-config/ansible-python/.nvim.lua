local dap = require("dap")
local overseer = require("overseer")
overseer.register_template({
	name = "[A] Run",
	builder = function(params)
		return {
			cmd = { "python" },
			args = { "main.py", "-p", "8001" },
		}
	end,
})
dap.configurations.python = {
	{
		-- The first three options are required by nvim-dap
		type = "python", -- the type here established the link to the adapter definition: `dap.adapters.python`
		request = "launch",
		name = "Launch",

		-- Options below are for debugpy, see https://github.com/microsoft/debugpy/wiki/Debug-configuration-settings for supported options

		program = "main.py", -- This configuration will launch the current file if used.
		args = { "-p", "8001" },

		pythonPath = function()
			-- debugpy supports launching an application with a different interpreter then the one used to launch debugpy itself.
			-- The code below looks for a `venv` or `.venv` folder in the current directly and uses the python within.
			-- You could adapt this - to for example use the `VIRTUAL_ENV` environment variable.
			local cwd = vim.fn.getcwd()

			if vim.fn.executable(os.getenv("VIRTUAL_ENV") .. "/bin/python") == 1 then
				return os.getenv("VIRTUAL_ENV") .. "/bin/python"
			elseif vim.fn.executable(os.getenv("HOME") .. "/.pyenv/shims/python") == 1 then
				vim.notify("python")
				return os.getenv("HOME") .. "/.pyenv/shims/python"
			elseif vim.fn.executable(cwd .. "/venv/bin/python") == 1 then
				vim.notify("python2")
				return cwd .. "/venv/bin/python"
			elseif vim.fn.executable(cwd .. "/.venv/bin/python") == 1 then
				vim.notify("python3")
				return cwd .. "/.venv/bin/python"
			else
				vim.notify("python4")
				return "/usr/bin/python"
			end
		end,
	},
}
