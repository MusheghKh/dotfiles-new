#!/bin/sh

# Install zsh plugins
~/Scripts/install/zsh_install_plugins.sh

if ! test -d ~/.fzf; then
  git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
  echo "Installing fzf"
  echo "HINT: Enable features (press 'y' 2 times) but reject config file changes (press 'n' 1 time)"
  ~/.fzf/install
fi

# In debian these programs has different names
if which batcat > /dev/null 2>&1  && ! test -e ~/.local/bin/bat; then
  ln -s "$(which batcat)" ~/.local/bin/bat
fi

if which fdfind > /dev/null 2>&1  && ! test -e ~/.local/bin/fd; then
  ln -s "$(which fdfind)" ~/.local/bin/fd
fi

curl https://zyedidia.github.io/eget.sh | sh

mkdir -p ~/progs

# Install neovim
eget neovim/neovim -t stable -a nvim-linux-x86_64.tar.gz -d
tar -xf nvim-linux-x86_64.tar.gz
mv nvim-linux-x86_64 ~/progs
cat <<DOC > ~/.local/bin/nvim
#!/bin/sh
exec ~/progs/nvim-linux-x86_64/bin/nvim "\$@"
DOC

# Install yazi
eget sxyazi/yazi -a yazi-x86_64-unknown-linux-musl --to ~/.local/bin/yazi

# Install bat
eget jesseduffield/lazygit --to ~/.local/bin/lazygit

# Install bat
eget sharkdp/bat -a x86_64-unknown-linux-gnu --to ~/.local/bin/bat

# Install delta
eget dandavison/delta -a x86_64-unknown-linux-gnu --to ~/.local/bin/delta

# Install tldr
eget tealdeer-rs/tealdeer --to ~/.local/bin/tldr

# Install k9s
eget derailed/k9s -a k9s_Linux_amd64.tar.gz --to ~/.local/bin/k9s

# Install fd
eget sharkdp/fd -a x86_64-unknown-linux-gnu --to ~/.local/bin/fd
