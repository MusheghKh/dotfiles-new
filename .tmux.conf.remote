##############
### Prefix ###
##############

# set prefix
# unbind C-b
# set -g prefix M-f 
# set -g prefix C-a

# nvim compatibility - fix the cursor problem
# set -g -a terminal-overrides ',*:Ss=\E[%p1%d q:Se=\E[2 q'

# set -ag terminal-overrides ",xterm-256color:RGB"
set -ga terminal-overrides ",*256col*:Tc"
# To have undercurls show up and in color, add the following to your Tmux configuration file
set -as terminal-overrides ',*:Smulx=\E[4::%p1%dm'  # undercurl support
set -as terminal-overrides ',*:Setulc=\E[58::2::%p1%{65536}%/%d::%p1%{256}%/%{255}%&%d::%p1%{255}%&%d%;m'  # underscore colours - needs tmux-3.0
# set-option -a terminal-overrides ",alacritty:RGB"

# set -g default-terminal "rxvt-256color";
# set -g default-terminal "tmux-256color"
set -g default-terminal "xterm-256color"
# set -g default-terminal "screen-256color"

# mouse mode
set -g mouse off

# Make a smaller delay so we can perform commands after switching windows
set -sg escape-time 0
set -sg repeat-time 600

# Window title begins by 1 and not 0
set -g base-index 0
setw -g pane-base-index 1

# Scrollback lines
set -g history-limit 10000

# Rather than constraining window size to the maximum size of any client 
# connected to the *session*, constrain window size to the maximum size of any 
# client connected to *that window*
setw -g aggressive-resize on

#vim key mode
set-window-option -g mode-keys vi

###################
### KEY BINDING ###
###################
# force a reload of the config file
unbind T
bind T source-file ~/.tmux.conf \; display "Reloaded ~/.tmux.conf"

# set window split
unbind v
unbind s
bind v split-window -h -c "#{pane_current_path}"
bind s split-window -c "#{pane_current_path}"

# vim pane selection
unbind h
unbind j
unbind k
unbind l
bind h select-pane -L
bind j select-pane -D
bind k select-pane -U
bind l select-pane -R

# pane resize
unbind Left
unbind Down
unbind Up
unbind Right
bind -r Left  resize-pane -L 5
bind -r Down  resize-pane -D 5
bind -r Up    resize-pane -U 5
bind -r Right resize-pane -R 5

# pane movement to window
# bind j command-prompt -p "join pane from:"  "join-pane -s '%%'"
# bind k command-prompt -p "send pane to:"  "join-pane -t '%%'"

unbind i
unbind I
bind-key   i choose-window 'join-pane    -s "%%"'
bind-key   I choose-window 'join-pane -h -s "%%"'

unbind O
unbind C-o
bind-key   O choose-window 'join-pane    -t "%%"'
bind-key   C-o choose-window 'join-pane -h -t "%%"'

unbind >
unbind <
bind-key > next-layout
bind-key < break-pane -d

unbind a
unbind A
bind a choose-tree -Zs
bind A choose-tree -Z

bind m \
    set -g mouse on \; \
    display 'Mouse: ON'

bind M \
    set -g mouse off \; \
    display 'Mouse: OFF'

# window management
# unbind n
unbind w
unbind W
unbind C-w
unbind r
bind W choose-tree -Zs 'move-window -t "%%"'
bind C-w choose-window 'swap-window -t "%%"'
bind r command-prompt "rename-window '%%'"

# panes
unbind \;
unbind ,
bind -r \; swap-pane -D       # swap current pane with the next one
bind -r , swap-pane -U       # swap current pane with the previous one

# Session management
unbind R
unbind S
bind R command-prompt "rename-session '%%'"
bind S command-prompt "new-session -s '%%'"

set -g set-clipboard on
unbind -T copy-mode-vi v
unbind -T copy-mode-vi y
bind -T copy-mode-vi v send-keys -X begin-selection
bind -T copy-mode-vi y send-keys -X copy-selection

# disable auto scroll down in copy mode
unbind -T copy-mode-vi MouseDragEnd1Pane

unbind C-s
bind-key C-s run-shell "$HOME/bin/tmux-edit-output.sh"
