setup_prompt() {
	local NORMAL_GREEN="\[\033[00;32m\]"
	local BOLD_YELLOW="\[\033[01;33m\]"
	# local NORNAL_YELLOW="\[\033[00;33m\]"
	local NORMAL_RED="\[\033[00;31m\]"
	# local BOLD_RED="\[\033[01;31m\]"
	local NORMAL_BLUE="\[\033[00;34m\]"
	local NC="\[\033[00m\]"

	local P_USER="$NORMAL_GREEN\u"
	local P_AT="$BOLD_YELLOW@"
	local P_HOST="$NORMAL_RED\h"
	local P_COLON="$BOLD_YELLOW:"
	local P_PATH="$NORMAL_BLUE\w"
	local P_SIGN="$BOLD_YELLOW \$ "

  local result=""
	result+="$P_USER"
	result+="$P_AT"
	result+="$P_HOST"
	result+="$P_COLON"
	result+="$P_PATH"
	result+="$P_SIGN"
	result+="$NC"

  PS1="$result"
}

HISTTIMEFORMAT="%Y-%m-%d %T "

setup_prompt

test -s ~/.config/bash/alias.bash && source ~/.config/bash/alias.bash || true
test -s ~/.config/bash/export.bash && source ~/.config/bash/export.bash || true
test -s ~/.config/bash/fzf.bash && source ~/.config/bash/fzf.bash || true
test -s ~/.config/bash/functions.bash && source ~/.config/bash/functions.bash || true

# use gpg-agent instead of ssh-agent
unset SSH_AGENT_PID
if [ "${gnupg_SSH_AUTH_SOCK_by:-0}" -ne $$ ]; then
	SSH_AUTH_SOCK="$(gpgconf --list-dirs agent-ssh-socket 2>/dev/null)"
	export SSH_AUTH_SOCK
fi

# gpg tty problem see Arch wiki GnuPG
GPG_TTY=$(tty)
export GPG_TTY
if pgrep -u "$USER" gpg-agent >/dev/null 2>&1; then
	gpg-connect-agent updatestartuptty /bye >/dev/null
fi

[ -s "$NVM_DIR/nvm.sh" ] && source "$NVM_DIR/nvm.sh" || true # This loads node version manager
# [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion" || true  # This loads node version manager bash_completion

# ghcup (Haskell)
[ -s ~/.ghcup/env ] && source ~/.ghcup/env || true # ghcup-env

# fzf
# global install
which fzf >/dev/null 2>&1 && source <(fzf --bash)
# local install
[ -f ~/.fzf.bash ] && source ~/.fzf.bash || true

load_kubeconfigs

which zoxide >/dev/null 2>&1 && eval "$(zoxide init bash)"

which direnv >/dev/null 2>&1 && eval "$(direnv hook bash)"

test -d ~/.miniforge3 && eval "$(~/.miniforge3/bin/conda shell.bash hook)"

export PYENV_ROOT="$HOME/.pyenv"
# [[ -d $PYENV_ROOT/bin ]] && export PATH="$PYENV_ROOT/bin:$PATH"
which pyenv >/dev/null 2>&1 && eval "$(pyenv init -)"
