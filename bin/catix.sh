#!/usr/bin/env bash

if [[ "$1" == "-g" ]]; then
  GEOMETRY="$2"
  shift 2
fi
for f in "$@"; do
  convert "$f" -geometry ${GEOMETRY:=800x480} sixel:-
done
