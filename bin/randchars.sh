#!/bin/sh

# $1 string lenght
# $2 can be only -n which will not print newline at the end

if [ "$#" -lt 1 ] ; then
	echo "[ERROR] string lenght is not specified" 1>&2
	echo "Usage:"
	printf "\t%s [-n] <number>\n" "$0"
	exit 1
fi

new_line=false
[ "$1" = "-n" ] && {
	new_line=true
	shift
}

alfa="a-zA-Z"
nums="0-9"
sym="!@#\$%^&*()_+[]{}:;\"'\\|,./<>?"

chars="$alfa"

printf "include numbers? (y/n) "
read -r num
[ "$num" = "y" ] || [ "$num" = "Y" ] && chars="$chars$nums"

printf "include symbols? (y/n) "
read -r num
[ "$num" = "y" ] || [ "$num" = "Y" ] && chars="$chars$sym"

result=$(tr -dc "$chars" < /dev/urandom | fold -w "$1" | head -n 1)

printf "%s" "$result"
"$new_line" && {
	printf "\n"
}
