#!/bin/sh

export XCURSOR_THEME='Breeze_Light'

if tmux ls | cut -d: -f1 | grep -q "^a$"; then
	exec wezterm start -- tmux attach -t a
	exit 0
fi

exec wezterm start
