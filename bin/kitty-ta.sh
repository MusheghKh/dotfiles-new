#!/bin/sh

if tmux ls | cut -d: -f1 | grep -q "^a$"; then
	exec kitty tmux attach -t a
	exit 0
fi

exec kitty
