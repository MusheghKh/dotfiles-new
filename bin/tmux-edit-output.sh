#!/bin/sh


# For Kitty terminal
# if [ -n "$KITTY" ]; then
#   kitty @ kitten ~/.local/share/nvim/lazy/kitty-scrollback.nvim/python/kitty_scrollback_nvim.py --env "TMUX=$TMUX" --env "TMUX_PANE=$(tmux display-message -p '#{pane_id}')"
#   exit
# fi

# For other terminals
scroll_position="$(tmux display-message -p '#{scroll_position}')"
cursor_y="$(tmux display-message -p '#{copy_cursor_y}')"
cursor_x="$(tmux display-message -p '#{copy_cursor_x}')"

if [ -z "$cursor_y" ]; then
	cursor_y="$(tmux display-message -p '#{cursor_y}')"
fi

if [ -z "$cursor_x" ]; then
	cursor_x="$(tmux display-message -p '#{cursor_x}')"
fi

pane_height="$(tmux display-message -p '#{pane_height}')"


file=$(mktemp)
tmux capture-pane -pS- > "$file"
if which nvim >/dev/null && [ -d ~/.dotfiles/.config/nvim ]; then
	# local session
	position='$'
	if [ -n "$cursor_y" ]; then
			buf_y_len="$(wc -l < "$file")"
			line_num="$(( pane_height - cursor_y - 1 ))"
			position="$(( buf_y_len - line_num))"
		if [ -n "$scroll_position" ]; then
			position="$(( position - scroll_position ))"
		fi
	fi

  cursor_x="$(( cursor_x + 1 ))"
	command="NVIM_APPNAME=nvim-flash nvim -c 'lua USER_EXTERNAL.flash_it($position, $cursor_x)' $file; rm $file"
	# command="nvim -c '$' $file; rm $file"
else
	command="vim '+$' $file; rm $file"
fi

tmux new-window -n:editbuffer "$command"
