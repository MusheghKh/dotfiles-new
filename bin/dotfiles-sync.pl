#!/usr/bin/env perl

use strict;
use warnings;

use Cwd 'abs_path';
use File::Basename;
use Env;

my $green  = "\033[00;32m";
my $blue   = "\033[00;34m";
my $yellow = "\033[00;33m";
my $red    = "\033[00;31m";
my $nc     = "\033[0m";

# change working directory where the dotfiles are
my $abs_path = abs_path($0);
my $dir      = dirname($abs_path);
chdir "$dir/..";

# Stash changes
print "${blue}Stashing existing changes...${nc}\n";
my $stash_result = `git stash push -m "sync-dotfiles: Before syncing dotfiles"`;
chomp($stash_result);
my $needs_pop = 1;
$needs_pop = 0 if ( $stash_result eq "No local changes to save" );

# git pull
print "${blue}Pulling updates from dotfiles repo...${nc}\n\n";
system("git pull") == 0 or die "git pull failed: $?";
print "\n";

# pop stash
if ($needs_pop) {
    print "${blue}Popping stashed changes...${nc}\n\n";
    system("git stash pop") == 0 or die "git stash pop failed: $?";
}

# check if there merge conflicts if yes terminate program
my $unmerged_files = `git diff --name-only --diff-filter=U`;
if ( $unmerged_files ne "" ) {
    print
"${red}The following files have merge conflicts after popping the stash:${nc}\n\n";
    print "$unmerged_files\n";
    exit 1;
}

# Create dirs that shouldn't be symlinks
print "\n${blue}Checking precreated directories${nc}\n";
my @precreated_dirs = (
    ".emacs.d",                 ".vim",
    ".kube",                    ".newsboat",
    ".zsh",                     ".gnupg",
    ".config/i3",               ".config/awesome",
    ".config/conky",            ".config/terminator",
    ".config/qBittorrent",      ".config/lazygit",
    ".config/bat/themes",       ".config/qt5ct/colors",
    ".config/k9s",              ".config/btop",
    ".config/yazi",             ".local/share/applications",
    ".local/share/rofi/themes", ".cache/nvim_servers",
    ".config/nvim/lua",         ".config/nvim-flash/lua"
);
foreach (@precreated_dirs) {
    my $home = $ENV{HOME};
    my $dir  = "$home/$_";
    if ( not -d "$dir" ) {
        print "Creating $dir\n";
        system("mkdir -p $dir") == 0 or die "Creating dir $dir failed: $?";
    }
}
print "\n";

# Run stow to ensure all new dotfiles are linked
print "Running stow .\n";
system("stow .") == 0 or die "stow . failed: $?";

my $arg_num = $#ARGV + 1;

if ( $arg_num > 0 && $ARGV[0] eq "full" ) {
    print "Updating lf colors and icons.\n";
    system(
"curl https://raw.githubusercontent.com/gokcehan/lf/master/etc/colors.example -o ~/.dotfiles/.config/lf/colors"
    );
    system(
"curl https://raw.githubusercontent.com/gokcehan/lf/master/etc/icons.example -o ~/.dotfiles/.config/lf/icons"
    );
}

