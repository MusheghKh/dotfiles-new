#!/bin/sh

set -e

command -v dotbot 1> /dev/null 2> /dev/null || python3 -m pip install --user dotbot

cd ~/.dotfiles || exit 1
git checkout .

git pull

dotbot -c ~/.dotfiles/dotbot.yaml

set +e

./dotbot_post_install.sh
